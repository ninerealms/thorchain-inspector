import click

import config
import util


########################################################################################
# Commands
########################################################################################


@click.group(cls=util.CustomGroup, help="check commands")
def checks():
    pass


@checks.group(help="thorchain internal checks")
def thorchain():
    pass


@checks.group(help="midgard consistency checks")
def midgard():
    pass


########################################################################################
# Thorchain Synths
########################################################################################


@thorchain.command("synths")
def thorchain_synths():
    """
    Prints results of Thorchain synth checks.
    """
    height = util.thornode_latest_height()

    balances = {}
    for coin in util.get(
        f"{config.THORNODE_API_ENDPOINT}/cosmos/bank/v1beta1/supply"
    ).json()["supply"]:
        balances[coin["denom"]] = coin

    for p in util.thorchain_pools(height):
        if int(p["synth_supply"]) == 0:
            continue
        amount = balances[p["asset"].lower().replace(".", "/")]["amount"]
        supply = p["synth_supply"]
        if amount != supply:
            click.secho(
                f"[FAIL] {p['asset']} amount={amount} supply={supply}", fg="red"
            )
        else:
            click.secho(f"[OK] {p['asset']}", fg="green")


########################################################################################
# Midgard Pools
########################################################################################


@midgard.command("pools")
@click.option("--threshold-percent", default=0.01)
def midgard_pools(threshold_percent):
    """
    Prints results of Midgard pool deviation checks.
    """
    height = util.thornode_latest_height()

    def check(tp, mp, tkey, mkey) -> bool:
        thornode = tp[tkey]
        midgard = mp[mkey]
        thornode = int(thornode)
        midgard = int(midgard)
        diff = thornode - midgard
        if diff == 0:
            return True
        diff_pct = (diff / max(thornode, midgard)) * 100
        if abs(diff_pct) < threshold_percent:
            return True
        asset = tp["asset"].split("-")[0]
        click.secho(
            f"[FAIL] {asset}:{mkey} diff={diff} diff_pct={diff_pct:.4f}%",
            fg="red",
        )
        return False

    # get pools
    tpools = {}
    for p in util.thorchain_pools(height):
        if p["status"] == "Suspended":
            continue
        tpools[p["asset"]] = p
    mpools = {}
    for p in util.midgard_pools():
        mpools[p["asset"]] = p

    # check that pool set matches
    for asset in set(tpools.keys()) - set(mpools.keys()):
        if "/" in asset:  # skip saver pools
            continue
        click.secho(f"Pool not found in Midgard: {asset}", fg="red")
    for asset in set(mpools.keys()) - set(tpools.keys()):
        click.secho(f"Pool not found in Thornode: {asset}", fg="red")

    # check pool values
    for asset, tp in tpools.items():
        if asset not in mpools:
            continue
        mp = mpools[asset]
        if all(
            [
                check(tp, mp, "balance_rune", "runeDepth"),
                check(tp, mp, "balance_asset", "assetDepth"),
                check(tp, mp, "LP_units", "liquidityUnits"),
                check(tp, mp, "pool_units", "units"),
                check(tp, mp, "synth_units", "synthUnits"),
                check(tp, mp, "synth_supply", "synthSupply"),
            ]
        ):
            click.secho(f"[OK] {tp['asset']}", fg="green")

    click.echo("\nNOTE: This check races and errors may be false positive.")
