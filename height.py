import click
import config
import datetime
from copy import deepcopy
from util import format_params, parse_blockcypher_time, get
import util


########################################################################################
# Helpers - Specific APIs
########################################################################################
def get_binance_height(ts: int, thornode_bnb_height: int = 1):
    # TODO: Need to refactor this with an API that will allow us to not rely on a
    # "close-enough" BNB height from THORNode to do interpolation/binary search.
    try:
        API_LIMIT = 50
        AVG_BLOCK_SEC = 0.43
        DELTA_PRECISION = round(API_LIMIT * AVG_BLOCK_SEC)
        params = {
            "startHeight": thornode_bnb_height,  # startHeight is inclusive.
            "endHeight": thornode_bnb_height + 1,  # endHeight is exclusive.
        }

        res = get(f"{config.BINANCE_API}/api/v1/blocks?{format_params(params)}")
        if res.status_code != 200:
            return None
        data = res.json()["blocks"][0]

        new_ts = data["time"] // 1000  # Binance endpoint is in microseconds
        new_height = thornode_bnb_height
        delta_sec = new_ts - ts

        # Variation on a midpoint search to narrow down precision to DELTA_PRECISION (~21 sec)
        while abs(delta_sec) > DELTA_PRECISION:
            delta_height = (delta_sec // AVG_BLOCK_SEC) * -1
            mid = new_height + delta_height
            low = int(mid - 25)
            high = int(mid + 25)
            params["startHeight"], params["endHeight"] = low, high
            res = get(f"{config.BINANCE_API}/api/v1/blocks?{format_params(params)}")
            if res.status_code != 200:
                break
            data = res.json()["blocks"]
            updated_height = False
            for block in data:
                if (block["time"] // 1000) >= ts:
                    new_ts = block["time"] // 1000
                    new_height = block["height"]
                    delta_sec = new_ts - ts
                    updated_height = True
                    break
            if updated_height is True:
                continue

            # If no height was updated, then all times are < ts
            new_ts = data[-1]["time"] // 1000
            delta_sec = new_ts - ts

        return ("BNB", new_height, "Binance")

    except Exception as e:
        print(f"Cannot get BNB data for timestamp: {ts}.")
        return None


def get_blockcypher_height(chain: str, ts: int, thornode_chain_height: int = 1):
    try:
        DELTA_PRECISION = 60 * 6  # Arbitrary 6 minutes representing a TC block.
        # If THORNode's observed chain height is within DELTA_PRECISION of
        # the chain's block timestamp, it's close enough.
        res = get(
            f"{config.BLOCKCYPHER_API}/v1/{chain.lower()}/main/blocks/{thornode_chain_height}"
        )
        data = res.json()
        thornode_ts = parse_blockcypher_time(data["time"])
        if abs(ts - thornode_ts.timestamp()) <= DELTA_PRECISION:
            return (chain, thornode_chain_height, "BlockCypher")

        # Otherwise, the chain height is too stale and we should search for
        # a closer block by using an interpolation search, starting from the
        # first and last observed block of the chain.
        res = get(f"{config.BLOCKCYPHER_API}/v1/{chain.lower()}/main")
        data = res.json()
        last_height = data["height"]
        last_ts = parse_blockcypher_time(data["time"])
        first_height = 1
        res = get(
            f"{config.BLOCKCYPHER_API}/v1/{chain.lower()}/main/blocks/{first_height}"
        )
        data = res.json()
        first_ts = parse_blockcypher_time(data["time"])
        delta_target = last_ts.timestamp() - ts

        while (
            abs(ts - first_ts.timestamp()) > DELTA_PRECISION
            or first_height < thornode_chain_height
        ):
            delta_range = last_ts.timestamp() - first_ts.timestamp()
            delta_pct = 1 - (delta_target / delta_range)
            first_height += int(delta_pct * (last_height - first_height))
            res = get(
                f"{config.BLOCKCYPHER_API}/v1/{chain.lower()}/main/blocks/{first_height}"
            )
            data = res.json()
            first_ts = parse_blockcypher_time(data["time"])

        return (chain, first_height, "BlockCypher")
    except:
        if data and data.get("error"):
            # This is most likely due to hitting the API limits for this API.
            print(f"BlockCypher API - {chain}: {data.get('error')}")
        else:
            print(f"Cannot get {chain} data for timestamp: {ts}")
        return None


def get_btc_dot_com_height(ts: int):
    try:
        source = "BTC.com"
        date = datetime.date.fromtimestamp(ts)
        url = f"{config.BTC_DOT_COM_API}/v3/block/date"
        res = get(f"{url}/{date.strftime('%Y-%m-%d')}")
        data = res.json()["data"]

        while data[-1]["timestamp"] > ts:
            date = date.__sub__(datetime.timedelta(days=1))
            res = get(f"{url}/{date.strftime('%Y-%m-%d')}")
            data = res.json()["data"]

        for block in data:
            if block["timestamp"] <= ts:
                return ("BTC", block["height"], source)

    except:
        print(f"Cannot get BTC data for timestamp: {ts}.")
        return None


def get_defillama_heights(chain: str, ts: int):
    try:
        source = "DefiLlama"
        res = get(
            f"{config.DEFILLAMA_API}/block/{config.DEFILLAMA_CHAIN_MAP[chain]}/{ts}"
        )
        data = res.json()
        return (chain, data["height"], source)

    except:
        print(f"Cannot get {chain} data for timestamp: {ts}")
        return None


def get_thornode_heights(tc_height: int):
    try:
        source = "THORNode"
        res = get(
            f"{config.THORNODE_API_ENDPOINT}/thorchain/lastblock?height={tc_height}"
        )
        if res.status_code != 200:
            return []
        chains = res.json()
        return [
            (c["chain"], c["last_observed_in"], source)
            for c in chains
            if c["chain"] not in config.RAGNAROK_CHAINS
        ]
    except:
        print(f"Cannot get THORNode data for height: {tc_height}")
        return None


def get_thor_height(v: int, k: str = "timestamp"):
    res = get(
        f"{config.MIDGARD_API_ENDPOINT}/v2/balance/{config.THORCHAIN_POOL_MODULE}?{k}={v}"
    )
    return res.json()


########################################################################################
# Helpers - Handlers/Wrappers
########################################################################################
def get_chain_heights_from_thorchain(v: int, k: str = "timestamp"):
    tc_height = v

    if k == "timestamp":
        data = get_thor_height(v, k)
        tc_height = int(data["height"])

    chain_heights = [("THOR", tc_height, "Midgard")]

    thornode = get_thornode_heights(tc_height)
    chain_heights.extend(thornode)

    return chain_heights


def get_chain_heights_from_third_parties(ts: int, chain_heights: list = []):
    seen = set([])

    # Overrides
    for i, chain_height in enumerate(chain_heights):
        chain, thornode_chain_height, _ = chain_height
        seen.add(chain)

        if chain == "BTC":
            new_chain_height = get_btc_dot_com_height(ts)
            if new_chain_height is not None:
                chain_heights[i] = new_chain_height

        if chain == "BNB":
            new_chain_height = get_binance_height(ts, thornode_chain_height)
            if new_chain_height is not None:
                chain_heights[i] = new_chain_height

        if chain in ["AVAX", "ETH"]:
            new_chain_height = get_defillama_heights(chain, ts)
            if new_chain_height is not None:
                chain_heights[i] = new_chain_height

        if chain in ["DOGE", "LTC"]:
            new_chain_height = get_blockcypher_height(chain, ts, thornode_chain_height)
            if new_chain_height is not None:
                chain_heights[i] = new_chain_height

    # Appends - Subset of third-party APIs
    if "BTC" not in seen:
        new_chain_height = get_btc_dot_com_height(ts)
        if new_chain_height is not None:
            chain_heights.append(new_chain_height)
        seen.add("BTC")

    for chain in ["AVAX", "ETH"]:
        if chain not in seen:
            new_chain_height = get_defillama_heights(chain, ts)
            if new_chain_height is not None:
                chain_heights.append(new_chain_height)
            seen.add(chain)

    for chain in ["DOGE", "LTC"]:
        if chain not in seen:
            new_chain_height = get_blockcypher_height(chain, ts)
            if new_chain_height is not None:
                chain_heights.append(new_chain_height)
            seen.add(chain)

    return chain_heights


def get_chain_heights(
    tc_height: int, ts: int, use_third_party_apis: bool, ignore_thorchain: bool
):
    chain_heights = []
    if ignore_thorchain is False:
        v = tc_height if tc_height is not None else ts
        k = "height" if tc_height is not None else "timestamp"
        chain_heights.extend(get_chain_heights_from_thorchain(v, k))

    if use_third_party_apis is True:
        chain_heights = get_chain_heights_from_third_parties(
            ts, deepcopy(chain_heights)
        )

    return sorted(chain_heights, key=lambda x: x[0])


def get_chain_heights_handler(
    tc_height: int,
    ts: int,
    date: str,
    use_third_party_apis: bool,
    ignore_thorchain: bool,
):
    # Data type validation
    if tc_height is not None:
        try:
            data = get_thor_height(tc_height, "height")
            ts = int(int(data["date"]) // 1e9)
        except:
            return print(f"THORChain height formatting is invalid: {tc_height}.")
    elif date is not None:
        try:
            ts = int(datetime.datetime.strptime(date, "%Y-%m-%d").timestamp())
        except:
            return print(f"Date formatting is invalid (YYYY-MM-DD): {date}.")

    # Genesis check
    if ts < config.GENESIS_HEIGHT_UNIX and ignore_thorchain is False:
        if tc_height is not None:
            return print(
                f"THORChain block height is less than genesis height ({config.GENESIS_HEIGHT}): {tc_height}"
            )
        else:
            return print(
                f"Timestamp is less than genesis timestamp ({config.GENESIS_HEIGHT_UNIX}): {ts}"
            )

    # Run logic
    try:
        if ignore_thorchain is True:
            use_third_party_apis = True

        chain_heights = get_chain_heights(
            tc_height, ts, use_third_party_apis, ignore_thorchain
        )

        if ignore_thorchain is False and tc_height is not None:
            print(
                f"For {datetime.datetime.fromtimestamp(ts)} (datetime), {ts} (unix), and {tc_height} (THORChain height):"
            )
        else:
            print(
                f"For {datetime.datetime.fromtimestamp(ts)} (datetime) and {ts} (unix):"
            )

        # Add formatting to align print layout.
        chain_pad = max([len(h[0]) for h in chain_heights])
        height_pad = max([len(str(h[1])) for h in chain_heights])
        for chain in chain_heights:
            print(
                f"Chain: {chain[0].ljust(chain_pad, ' ')} | Height: {str(chain[1]).ljust(height_pad, ' ')} | Source: {chain[2]}"
            )
    except Exception as e:
        print(e)


########################################################################################
# Commands
########################################################################################


@click.group(
    cls=util.CustomGroup,
    help="commands to give nearest block height for a specified chain based on unix timestamp",
)
def height():
    pass


########################################################################################
# Height
########################################################################################


@height.command(help="provides exact or closest block height based on unix timestamp")
@click.argument("ts", type=int)
@click.option(
    "--use-third-party-apis",
    default=False,
    type=bool,
    help="Use third-party APIs as sources (slower)",
)
@click.option(
    "--ignore-thorchain",
    default=False,
    type=bool,
    help="Ignore THORNode and Midgard (to avoid genesis height limit)",
)
def ts(ts, use_third_party_apis, ignore_thorchain):
    get_chain_heights_handler(
        tc_height=None,
        ts=ts,
        date=None,
        use_third_party_apis=use_third_party_apis,
        ignore_thorchain=ignore_thorchain,
    )


@height.command(help="provides exact or closest block height based on YYYY-MM-DD")
@click.argument("date", type=str)
@click.option(
    "--use-third-party-apis",
    default=False,
    type=bool,
    help="Use third-party APIs as sources (slower)",
)
@click.option(
    "--ignore-thorchain",
    default=False,
    type=bool,
    help="Ignore THORNode and Midgard (to avoid genesis height limit)",
)
def date(date, use_third_party_apis, ignore_thorchain):
    get_chain_heights_handler(
        tc_height=None,
        ts=None,
        date=date,
        use_third_party_apis=use_third_party_apis,
        ignore_thorchain=ignore_thorchain,
    )


@height.command(
    help="provides exact or closest block height based THORChain block height"
)
@click.argument("tc_height", type=int)
@click.option(
    "--use-third-party-apis",
    default=False,
    type=bool,
    help="Use third-party APIs as sources (slower)",
)
def tc_height(tc_height, use_third_party_apis):
    get_chain_heights_handler(
        tc_height=tc_height,
        ts=None,
        date=None,
        use_third_party_apis=use_third_party_apis,
        ignore_thorchain=False,
    )
