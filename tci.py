import sys

import click

import address
import block
import config
import checks
import chains
import chart
import invariants
import network
import nodes
import runepool
import txs
import vaults
import height
import util  # must import to export


#########################################################################################
# Init
########################################################################################


# Override the system exception hook to drop into a debugger when exceptions are raised.
def info(type, value, tb):
    if hasattr(sys, "ps1") or not sys.stderr.isatty():
        sys.__excepthook__(type, value, tb)
    else:
        import traceback, pdb

        traceback.print_exception(type, value, tb)
        pdb.pm()


sys.excepthook = info


def switch_stagenet():
    config.NETWORK = "stagenet"
    config.THORNODE_API_ENDPOINT = "https://stagenet-thornode.ninerealms.com"
    config.THORNODE_RPC_ENDPOINT = "https://stagenet-rpc.ninerealms.com"
    config.MIDGARD_API_ENDPOINT = "https://stagenet-midgard.ninerealms.com"
    config.GENESIS_HEIGHT = 627201


def switch_mocknet():
    config.NETWORK = "mocknet"
    config.THORNODE_API_ENDPOINT = "http://localhost:1317"
    config.THORNODE_RPC_ENDPOINT = "http://localhost:26657"
    config.MIDGARD_API_ENDPOINT = "http://localhost:8080"
    config.GENESIS_HEIGHT = 1


@click.group(cls=util.CustomGroup)
@click.option("--stagenet/--no-stagenet", default=False, help="switch to stagenet")
@click.option("--mocknet/--no-mocknet", default=False, help="switch to mocknet")
@click.option("--midgard", default="", help="override midgard endpoint")
@click.option("--thornode", default="", help="override thornode endpoint")
@click.option("--rpc", default="", help="override rpc endpoint")
def cli(stagenet, mocknet, midgard, thornode, rpc):
    if stagenet:
        switch_stagenet()
    if mocknet:
        switch_mocknet()

    if midgard:
        config.MIDGARD_API_ENDPOINT = midgard
    if thornode:
        config.THORNODE_API_ENDPOINT = thornode
    if rpc:
        config.THORNODE_RPC_ENDPOINT = rpc


########################################################################################
# Commands
########################################################################################

cli.add_command(address.address)
cli.add_command(block.block)
cli.add_command(chains.chains)
cli.add_command(chart.chart)
cli.add_command(checks.checks)
cli.add_command(height.height)
cli.add_command(invariants.invariants)
cli.add_command(network.network)
cli.add_command(nodes.nodes)
cli.add_command(runepool.runepool)
cli.add_command(txs.txs)
cli.add_command(vaults.vaults)
