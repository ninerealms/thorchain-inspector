import datetime
import time

import click
import tqdm
import pandas as pd
import plotext as plt

import config
import util


########################################################################################
# Commands
########################################################################################


@click.group(cls=util.CustomGroup, help="commands to connected chains")
def chains():
    pass


########################################################################################
# Gas
########################################################################################


@chains.command()
@click.argument("chain", type=str)
@click.option("--count", type=int, default=20, help="count of sample blocks")
@click.option("--start", type=int, default=-1000, help="start block")
@click.option("--end", type=int, default=-1, help="end block")
def gas(chain, count, start, end):
    """
    Print Thorchain gas price for the provided chain.
    """
    # allow reverse index from tip
    latest = util.thornode_latest_height()
    if start < 0:
        start = latest + start
    if end < 0:
        end = latest + end

    # block time for estimated times
    block_time = util.average_block_time()

    rows = []
    units = ""

    for height in tqdm.tqdm(range(start, end + 1, (1 + end - start) // count)):
        for chain_data in util.thorchain_inbound_addresses(height):
            if chain_data["chain"].lower() == chain.lower():
                units = chain_data["gas_rate_units"]
                est_block_time = time.time() - (latest - height) * block_time
                dt = datetime.datetime.fromtimestamp(est_block_time)
                fmt_time = dt.strftime("%Y-%m-%d %H:%M")
                rows.append(
                    {
                        "height": f"[{height}] {fmt_time}",
                        "gas": int(chain_data["gas_rate"]),
                    }
                )

    df = pd.DataFrame(rows)
    click.echo(f"Gas price in {units} for {chain} chain:")
    plt.simple_bar(df["height"], df["gas"])
    plt.show()


########################################################################################
# Txs
########################################################################################


@chains.command()
@click.argument("chain", type=str)
@click.argument("address", type=str)
@click.option("--limit", type=int, default=1, help="limit")
def txs(chain, address, limit):
    """
    Print txids for the provided chain and address.
    """
    api = config.SHAPESHIFT_APIS.get(chain.lower())
    if not api:
        click.echo(f"Supported chains: {', '.join(config.SHAPESHIFT_APIS.keys())}")
        return

    url = f"{api}/api/v1/account/{address}/txs"
    params = {"pageSize": 100}
    txids = []

    # iterate paged results with cursor
    while len(txids) < limit:
        response = util.get(url, params=params)
        if response.status_code != 200:
            click.echo(f"Error: {response.status_code} {response.text}")
            break

        data = response.json()
        txids.extend([tx["txid"] for tx in data["txs"]])
        if not data.get("cursor"):
            break

        params["cursor"] = data["cursor"]

        click.echo(f"Found {len(txids)} txids...", err=True)

    # print all txids
    for txid in txids[:limit]:
        click.echo(txid)
