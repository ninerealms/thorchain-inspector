import functools
import math
import time

import click
import pandas as pd
import semver
import tqdm

import config
import util

########################################################################################
# Commands
########################################################################################


@click.group(cls=util.CustomGroup, help="commands to investigate network")
def network():
    pass


########################################################################################
# Queue
########################################################################################


@network.command()
@click.option("--height", help="height to determine queue", type=int)
@click.option(
    "--verbose-clout", help="determine clout needed/used/available", is_flag=True
)
def queue(height, verbose_clout):
    """
    Print current scheduled and outbound queue.
    """
    height = height or util.thornode_latest_height()

    rows = []
    for queue in ["outbound", "scheduled"]:
        res = util.get(
            f"{config.THORNODE_API_ENDPOINT}/thorchain/queue/{queue}",
            params={"height": height},
        )
        for item in res.json():
            rows.append(
                {
                    "asset": item["coin"]["asset"],
                    "height": item["height"],
                    "age": int(res.headers["X-Thorchain-Height"]) - item["height"],
                    "amount": int(item["coin"]["amount"]),
                    "type": queue,
                    "memo": item["memo"],
                    "clout_spent": int(item.get("clout_spent", 0)),
                    "in_hash": item["in_hash"],
                    "to_address": item["to_address"],
                }
            )

    if len(rows) == 0:
        click.echo("No items in queue")
        return

    @functools.cache
    def get_clout(address):
        return util.get(
            f"{config.THORNODE_API_ENDPOINT}/thorchain/clout/swap/{address}",
            params={"height": height},
        ).json()

    if verbose_clout:
        pools = {p["asset"]: p for p in util.thorchain_pools(height)}

        for row in tqdm.tqdm(rows):
            inbound = util.get(
                f"{config.THORNODE_API_ENDPOINT}/thorchain/tx/{row['in_hash']}"
            )
            from_address = inbound.json()["observed_tx"]["tx"]["from_address"]
            clout_in = get_clout(from_address)
            clout_out = get_clout(row["to_address"])
            clout_available = int(clout_in["score"]) + int(clout_out["score"])
            clout_available += int(clout_in["reclaimed"]) + int(clout_out["reclaimed"])
            clout_available -= int(clout_in["spent"]) + int(clout_out["spent"])
            row["clout_available"] = clout_available
            row["rune_value"] = (
                row["amount"]
                * int(pools[row["asset"]]["balance_rune"])
                / int(pools[row["asset"]]["balance_asset"])
            )
            row["total_clout"] = int(clout_in["score"]) + int(clout_out["score"])

    for row in rows:
        del row["in_hash"]
        del row["to_address"]

    df = pd.DataFrame(rows)
    df = df.assign(asset=df.asset.apply(lambda x: x.split("-")[0]))
    df = df.assign(clout_spent=df.clout_spent.apply(lambda x: f"{x/1e8:,.2f}"))

    if verbose_clout:
        df.sort_values(by=["rune_value"], inplace=True, ascending=False)
        df = df.assign(rune_value=df.rune_value.apply(lambda x: f"{x/1e8:,.2f}"))
        df = df.assign(
            clout_available=df.clout_available.apply(lambda x: f"{x/1e8:,.2f}")
        )
        df = df.assign(total_clout=df.total_clout.apply(lambda x: f"{x/1e8:,.2f}"))
        df = df[
            [
                "asset",
                "amount",
                "type",
                "memo",
                "rune_value",
                "clout_spent",
                "clout_available",
                "total_clout",
            ]
        ]

    click.echo(df.to_string(index=False))


########################################################################################
# Stuck Outbounds
########################################################################################


@network.command()
@click.option("--threshold", help="block age to consider stuck", type=int, default=900)
@click.option("--chains", help="comma separated chains to consider")
@click.option("--height", help="height to determine stuck outbounds", type=int)
def stuck_outbounds(threshold, chains, height):
    """
    Print current stuck outbounds.
    """
    chains = chains.upper().split(",") if chains else None
    height = height or util.thornode_latest_height()
    queue = util.get(
        f"{config.THORNODE_API_ENDPOINT}/thorchain/queue/outbound",
        params={"height": height} if height else None,
    ).json()
    vaults = util.thorchain_vaults_asgard(height)
    vaults_balances = {
        v["pub_key"]: {c["asset"]: float(c["amount"]) / 1e8 for c in v["coins"]}
        for v in vaults
    }

    rows = []
    for item in tqdm.tqdm(queue):
        if chains and item["coin"]["asset"].split(".")[0] not in chains:
            continue

        # get inbound
        inbound = util.get(
            f"{config.THORNODE_API_ENDPOINT}/thorchain/tx/details/{item['in_hash']}"
        )
        if inbound.status_code != 200:
            click.echo(f"Failed to get tx: {item['in_hash']}")
            continue
        rows.append(
            {
                "asset": item["coin"]["asset"].split("-")[0],
                "amount": float(item["coin"]["amount"]) / 1e8,
                "age": height - inbound.json()["finalised_height"],
                "scheduled": item["height"],
                "vault": item["vault_pub_key"][-4:],
                "vault_balance": vaults_balances.get(item["vault_pub_key"], {}).get(
                    item["coin"]["asset"]
                ),
                "to_address": f'...{item["to_address"][-4:]}',
                "in_hash": f'{item["in_hash"]}',
            }
        )

    df = pd.DataFrame(rows)
    df = df[df["age"] > threshold]

    # if empty no stuck outbounds
    if df.empty:
        click.echo("No stuck outbounds")
        return

    df.sort_values(by=["scheduled"], inplace=True, ascending=False)
    click.echo(df.to_string(index=False))

    # print count by asset
    click.echo()
    click.secho("Total by Asset:", reverse=True)
    click.echo(df.groupby("asset").size().to_string(header=False))

    # print count by vault
    click.echo()
    click.secho("Total by Vault:", reverse=True)
    click.echo(df.groupby("vault").size().to_string(header=False))


########################################################################################
# Height
########################################################################################


@network.command()
def height():
    """
    Print current height.
    """
    height = util.thornode_latest_height()
    click.echo(height)


########################################################################################
# Find Upgrade Height
########################################################################################


@network.command()
@click.argument("version")
def find_upgrade_height(version):
    """
    Finds the height of the network version upgrade.
    """
    low = config.GENESIS_HEIGHT
    high = util.thornode_latest_height()
    check = low

    while high - low > 1:
        check = (low + high) // 2

        # blacklist bad height range - not sure why the version on these is wrong
        if check < 6640000 and check > 6575965:
            check = 6640000

        res = util.get(
            f"{config.THORNODE_API_ENDPOINT}/thorchain/version",
            params={"height": check},
        ).json()

        if semver.compare(res.get("current"), version) < 0:
            low = check
        else:
            high = check

    # also get block time
    res = util.get(
        f"{config.THORNODE_RPC_ENDPOINT}/block", params={"height": high}
    ).json()
    time = res["result"]["block"]["header"]["time"]

    click.echo(f"Version upgrade {version} at height {check}: {time}")


########################################################################################
# Watch Churn
########################################################################################


@network.command("watch-churn")
def watch_churn():
    """
    Watches a countdown to the next churn.
    """

    def height():
        res = util.get(f"{config.THORNODE_RPC_ENDPOINT}/status").json()
        return int(res["result"]["sync_info"]["latest_block_height"])

    churn_height = int(util.midgard_network().get("nextChurnHeight"))
    click.echo(f"Next churn at height {churn_height}")
    last = height()

    with tqdm.tqdm(total=churn_height - last, unit="block", smoothing=0.01) as bar:
        while True:
            time.sleep(1)
            current = height()
            bar.update(current - last)
            last = current


########################################################################################
# Churn Heights
########################################################################################


@network.command("churn-heights")
@click.option("--churns", help="the number of past churns to inspect", default=3)
@click.option("--height", help="the ending height", type=int)
def churn_heights(churns, height):
    """
    Prints the heights of recent churns.
    """
    for height in util.churn_heights(churns, height):
        click.echo(f"Churn at height {height}")


########################################################################################
# Module Balance Changes
########################################################################################


@network.command("module-balance-changes")
@click.argument("module")
@click.option("--start", help="the start height", type=int)
@click.option("--end", help="the end height", type=int)
def module_balance_changes(module, start, end):
    """
    Prints the module balance changes for heights in the range.
    """
    balance = None
    for height in range(start, end):
        new_balance = util.thorchain_module_balance_rune(module, height)
        if balance is not None:
            click.secho(
                f"{height}: {new_balance - balance:,.10f}",
                fg="green"
                if new_balance > balance
                else "red"
                if new_balance < balance
                else "white",
            )
        balance = new_balance


########################################################################################
# Network Version Summary
########################################################################################


@network.command("version")
@click.option("--height", help="query at the provided height")
def get_version(height):
    """
    Retrieves the version field from active nodes and prints a summary of the count of nodes on each unique version.
    """
    nodes = util.thorchain_nodes(height)

    version_counts = {}

    for node in nodes:
        if node["status"] == "Active":
            version = node.get("version")
            if version:
                if version in version_counts:
                    version_counts[version] += 1
                else:
                    version_counts[version] = 1

    if version_counts:
        for version, count in version_counts.items():
            click.echo(f"Version {version}: {count} nodes")
    else:
        click.echo("No active nodes found.")


########################################################################################
# Network Pendumum Summary
########################################################################################


@network.command()
@click.option("--height", help="query at the provided height")
def pendulum(height):
    """
    Prints metrics regarding the THORChain incentive pendulum.
    """
    nodes = util.thorchain_nodes(height)
    pools = util.thorchain_pools(height)
    vaults = util.thorchain_vaults_asgard(height)

    total_secured_value = 0
    active_bond = 0

    # Fetch network mimir or constant values
    mimir = util.thornode_mimir(height)
    constants = util.thorchain_constants(height)

    # Set values, using mimir if available, otherwise fall back to constants
    asset_bps = int(
        mimir.get(
            "PENDULUMASSETBASISPOINTS",
            constants.get("int_64_values", {}).get("PendulumAssetsBasisPoints",10000),
            
        )
    )
    use_vault_assets = bool(
        mimir.get(
            "PENDULUMUSEVAULTASSETS",
            constants.get("int_64_values", {}).get("PendulumUseVaultAssets", False),
        )
    )
    effective_security = bool(
        mimir.get(
            "PENDULUMUSEEFFECTIVESECURITY",
            constants.get("int_64_values", {}).get(
                "PendulumUseEffectiveSecurity", False
            ),
        )
    )

    assetTotals = {}
    poolPrices = {}

    # Calculate total amounts for each asset across all vaults
    for v in vaults:
        for c in v["coins"]:
            asset = c["asset"]
            amount = int(c["amount"]) / 1e8

            assetTotals[asset] = (assetTotals.get(asset, False) or 0) + amount

    # Get asset prices in RUNE
    for p in pools:
        if p["status"] == "Available":
            assetPrice = int(p["balance_rune"]) / int(p["balance_asset"])
            poolPrices[p["asset"]] = assetPrice
            total_secured_value += int(p["balance_rune"]) / 1e8

    # Calculate total secured value based on PendulumUseVaultAssets mimir
    if use_vault_assets:
        total_secured_value = 0
        # Use vault assets (existing logic)
        for asset, amount in assetTotals.items():
            runeValue = amount * (poolPrices.get(asset, False) or 0)
            total_secured_value += runeValue

    # Filter to active nodes and calculate total bond
    bonds = []
    for n in nodes:
        if n["status"] == "Active":
            bonds.append(int(n["total_bond"]))

    if effective_security:
        bonds.sort(reverse=True)  # Sort all bond values in descending order
        splitIndex = math.floor(len(bonds) * 2 / 3)
        active_bond = int(sum(bonds[splitIndex:]) / 1e8) # Use the bottom two-thirds of bond values 
    else:
        active_bond = int(sum(bonds) / 1e8)

    # Calculcate adjusted secured assets
    adjusted_secured_value = total_secured_value * (asset_bps / 10000)

    # Calculate node and pool share
    # poolShare = (b - p) / b
    pool_share = (active_bond - adjusted_secured_value) / + active_bond
    node_share = 1 - pool_share

    # Determine network state
    if abs(node_share - 50) < 0.1:
        network_state = "Normal"
    elif node_share < 50:
        network_state = "Overbonded"
    else:
        network_state = "Underbonded"

    click.echo(f"           Reward Share | Total Value")
    click.echo(f"Bond:      {(node_share*100):0.2f}%       | {active_bond:,}")
    click.echo(
        f"Liquidity: {(pool_share*100):0.2f}%       | {int(adjusted_secured_value):,}"
    )
    click.echo(f"\nNetwork is {network_state}")
    click.echo(
        f"\nPendulum Parameters:\nPENDULUMASSETBASISPOINTS: {asset_bps} \nPENDULUMUSEVAULTASSETS: {use_vault_assets} \nPENDULUMUSEEFFECTIVESECURITY: {effective_security}"
    )
