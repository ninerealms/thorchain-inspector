import concurrent.futures
import collections
import math

import click
import pandas as pd
import tqdm

import util


########################################################################################
# Commands
########################################################################################


@click.group(cls=util.CustomGroup, help="commands to investigate vaults")
@click.option("--height", type=int, help="query at the provided height")
@click.pass_context
def vaults(ctx, height):
    ctx.ensure_object(dict)
    ctx.obj["height"] = height


########################################################################################
# Status
########################################################################################


@vaults.command("status")
@click.option("--asset", help="asset to query")
@click.option("--tokens/--no-tokens", default=False, help="whether to include tokens")
@click.option("--retiring/--no-retiring", help="show non-zero retiring balances")
@click.option(
    "--long-key/--short-key", help="print the entire vault pub key", default=False
)
@click.pass_context
def status(ctx, asset, tokens, retiring, long_key):
    """
    Prints status for all vaults of the provided type (asgard or yggdrasil).
    """
    height = ctx.obj["height"] or util.thornode_latest_height()
    rows = []
    vaults = util.thorchain_vaults_asgard(height)

    # best effort to calculate age
    try:
        block_time = util.average_block_time()
    except:
        block_time = 5

    for v in vaults:
        age = (height - v["block_height"]) * block_time
        days = int(age // (24 * 60 * 60))
        hours = int((age % (24 * 60 * 60)) // (60 * 60))
        minutes = int((age % (60 * 60)) // 60)

        row = {
            "pub_key": v["pub_key"],
            "status": v["status"],
            "height": v["block_height"],
            "age": f"{days}d{hours}h{minutes}m",
        }
        if not long_key:
            row["pub_key"] = row["pub_key"][:8] + "..." + row["pub_key"][-4:]

        # output amounts for gas assets
        for c in v["coins"] or []:
            if asset:
                if c["asset"].startswith(asset):
                    row[c["asset"]] = int(c["amount"]) / 1e8
            elif tokens or retiring:
                row[c["asset"].split("-")[0]] = int(c["amount"]) / 1e8
            elif c["asset"].split(".")[0] == c["asset"].split(".")[1] or c["asset"] in {
                "GAIA.ATOM",
                "BSC.BNB",
                "BASE.ETH",
            }:
                row[c["asset"]] = int(c["amount"]) / 1e8

        rows.append(row)

    df = pd.DataFrame(rows).set_index("pub_key")

    # if --retiring drop active vaults and show non-zero retiring balances
    if retiring:
        df = df[df.status == "RetiringVault"]
        df = df.loc[:, df.columns[df.sum() != 0]]

    print(df.to_string())


########################################################################################
# Node Membership
########################################################################################


@vaults.command("node-membership")
@click.option("--vault", help="filter a specific vault pubkey")
@click.option("--country/--no-country", default=False, help="show node country")
@click.pass_context
def node_membership(ctx, vault, country):
    """
    Prints node membership by vault.
    """
    height = ctx.obj["height"] or util.thornode_latest_height()

    # gather vault nodes
    rows = []
    for node in util.thorchain_nodes(height):
        if vault and vault not in (node["signer_membership"] or []):
            continue
        if node["status"] not in {"Active", "Standby"}:
            continue
        row = {
            "node_address": node["node_address"],
            "vault": vault or node["signer_membership"][-1],
            "operator": node["node_operator_address"],
            "ip_address": node["ip_address"],
            "status": node["status"],
        }
        rows.append(row)

    df = pd.DataFrame(rows)

    # gather node countries
    def get_country(ip):
        try:
            res = util.geocode(ip)
            return res["country"]
        except:
            return ""

    if country:
        with concurrent.futures.ThreadPoolExecutor(32) as p:
            futures = df.ip_address.apply(lambda ip: p.submit(get_country, (ip)))
            df = df.assign(country=futures.apply(lambda f: f.result()))

    # print dataframe
    df.drop(columns=["ip_address"], inplace=True)
    df.vault = df.vault.apply(lambda x: x[-4:])
    idx = ["vault", "status", "operator"]
    df.operator = df.operator.apply(lambda x: x[-4:])
    if country:
        idx = idx[:1] + ["country"] + idx[1:]
    df.set_index(idx, inplace=True)
    df.sort_index(inplace=True)
    click.echo(df.to_string())

    # get vault to determine full member count
    if vault:
        vault = util.thorchain_vault(vault, height)
        members = len(vault["membership"])
        percent = float(len(df)) / members * 100
        click.echo()
        click.echo(f"{len(df)}/{members} ({percent:.1f}%) members available")


########################################################################################
# Vault Distribution
########################################################################################


@vaults.command("distribution-by-type")
@click.pass_context
def distribution_by_type(ctx):
    """
    Prints the vault distribution by vault type.
    """
    height = ctx.obj["height"] or util.thornode_latest_height()
    asgard = util.thorchain_vaults_asgard(height)
    ygg = util.thorchain_vaults_yggdrasil(height)

    vaults = collections.defaultdict(lambda: collections.defaultdict(lambda: 0))
    for vault in asgard:
        for coin in vault["coins"]:
            vaults[coin["asset"]]["asgard"] += int(coin["amount"])
            vaults[coin["asset"]][f"asgard-{vault['status']}"] += int(coin["amount"])

    for vault in ygg:
        for coin in vault["coins"]:
            vaults[coin["asset"]]["ygg"] += int(coin["amount"])
            vaults[coin["asset"]][f"ygg-{vault['status']}"] += int(coin["amount"])

    rows = []
    for pool, vaults in vaults.items():
        rows.append({"pool": pool} | vaults)
    df = pd.DataFrame(rows)
    df = df.fillna(0)

    total = df.asgard + df.ygg
    for col in set(df.columns) - {"pool"}:
        df[col] = (df[col] / total * 100).round(decimals=2)

    df["pool"] = df.pool.apply(lambda p: p.split("-")[0])

    df = df.set_index("pool").sort_values(by="ygg", ascending=False)
    print(df.to_string())


########################################################################################
# Distribution
########################################################################################


@vaults.command("distribution-by-node")
@click.option("--asset", help="filter a specific asset")
@click.pass_context
def distribution_by_node(ctx, asset):
    """
    Prints the vault distribution by node.
    """
    height = ctx.obj["height"] or util.thornode_latest_height()
    if asset:
        asset = asset.upper()
    rows = []
    for vault_type in ["asgard", "yggdrasil"]:
        if vault_type == "asgard":
            vaults = util.thorchain_vaults_asgard(height)
        else:
            vaults = util.thorchain_vaults_yggdrasil(height)
        for v in vaults:
            for c in v["coins"]:
                addr = ""
                for a in v["addresses"]:
                    if a["chain"] == c["asset"].split(".")[0]:
                        addr = a["address"]
                rows.append(
                    {
                        "asset": c["asset"].split("-")[0],
                        "type": vault_type,
                        "status": v["status"],
                        "addr": addr,
                        "amount": int(c["amount"]),
                    }
                )

    df = pd.DataFrame(rows)
    df = df.set_index("addr")

    if asset:
        df = df[df.asset == asset]
        df = df.assign(pct=(df.amount / df.amount.sum() * 100).round(decimals=2))
        df = df.sort_values(by="pct", ascending=False)

    print(df.to_string())


########################################################################################
# Balance Changes
########################################################################################


@vaults.command("balance-changes")
@click.argument("pubkey")
@click.option("--asset", help="vault asset")
@click.option("--start", type=int, help="start block")
@click.option("--end", type=int, help="end block")
def balance_changes(pubkey, asset, start, end):
    """
    Prints the vault balance changes for the provided asset over the block range.
    """
    balance = None
    for height in range(start, end):
        vault = util.thorchain_vault(pubkey, height)
        for coin in vault["coins"]:
            if coin["asset"] == asset:
                new_balance = int(coin["amount"])
                if balance is not None:
                    click.secho(
                        f"{height}: {(new_balance - balance)/1e8} {asset}",
                        fg=(
                            "green"
                            if new_balance > balance
                            else "red" if new_balance < balance else "white"
                        ),
                    )
                balance = new_balance
                break


########################################################################################
# Security
########################################################################################


@vaults.command("security")
@click.pass_context
def security(ctx):
    """
    Prints the vault security statistics.
    """
    height = ctx.obj["height"] or util.thornode_latest_height()

    nodes = {}
    for node in util.thorchain_nodes(height):
        nodes[node["node_address"]] = node

    # map pub key to node address
    pk_node = {}
    for node in nodes.values():
        pk_node[node.get("pub_key_set", {}).get("secp256k1", "")] = node

    # get asgard vaults
    vault_bonds = collections.defaultdict(list)
    asgards = util.thorchain_vaults_asgard(height)
    for a in asgards:
        for pk in a["membership"]:
            vault_bonds[a["pub_key"]].append(int(pk_node[pk]["total_bond"]))

    # get pools
    pools = {}
    for pool in util.thorchain_pools(height):
        pools[pool["asset"]] = pool

    # determine rune value of vault
    rune_value = collections.defaultdict(float)
    for vault in asgards:
        for coin in vault["coins"]:
            if coin["asset"] not in pools:
                continue
            value = (
                int(coin["amount"])
                * int(pools[coin["asset"]]["balance_rune"])
                / int(pools[coin["asset"]]["balance_asset"])
            )
            rune_value[vault["pub_key"]] += value / 1e8

    # build dataframe
    rows = []
    for vault, bonds in vault_bonds.items():
        bonds = sorted(bonds)
        rows.append(
            {
                "vault": vault[-4:],
                "min_quorum_bond": sum(bonds[: math.ceil(len(bonds) * 2 / 3)]) / 1e8,
                "total_bond": sum(bonds) / 1e8,
                "rune_value": rune_value[vault],
                "quorum": math.ceil(len(bonds) * 2 / 3),
                "members": len(bonds),
            }
        )

    df = pd.DataFrame(rows)
    df = df.set_index("vault")
    df = df.sort_values(by="rune_value", ascending=False)
    df.min_quorum_bond = df.min_quorum_bond.round(decimals=2)
    df.total_bond = df.total_bond.round(decimals=2)
    df.rune_value = df.rune_value.round(decimals=2)
    df = df.assign(security=df.min_quorum_bond / df.rune_value)
    click.echo(df.to_string())


########################################################################################
# Retired
########################################################################################


@vaults.command("retired")
@click.option("--churns", help="the number of past churns to inspect", default=3)
@click.option(
    "--long-key/--short-key", help="print the entire vault pub key", default=False
)
@click.pass_context
def retired(ctx, churns, long_key):
    """
    Prints retired vault balances.
    """
    last_height = ctx.obj["height"] or util.thornode_latest_height()
    last_height = int(last_height)
    retired_vaults = {}

    # get vault addresses for the last n churns worth of vaults
    click.echo(f"getting vault addresses for the last {churns} churns...")
    for i in tqdm.tqdm(range(0, churns + 1)):
        vaults = util.thorchain_vaults_asgard(last_height)
        for v in vaults:
            if v["block_height"] < last_height:
                last_height = v["block_height"]
            if i == 0:
                continue
            retired_vaults[v["pub_key"]] = {
                "pub_key": v["pub_key"],
                "height": v["block_height"],
                "addresses": v["addresses"],
            }

    # get the time for all vaults
    click.echo(f"getting time for {len(retired_vaults)} vaults...")
    for v in retired_vaults.values():
        v["time"] = util.thorchain_height_to_date(v["height"])

    # rows for dataframe to print later
    rows = []

    # get the balances for all vaults
    click.echo(f"getting chain balances for {len(retired_vaults)} vaults...")
    for v in tqdm.tqdm(retired_vaults.values()):
        v = {
            **v,
            **{
                a["chain"]: util.get_balance(a["chain"], a["address"])
                for a in v["addresses"]
                if a["chain"] != "THOR"
            },
        }
        del v["addresses"]
        rows.append(v)

    # format the dataframe
    df = pd.DataFrame(rows)
    if not long_key:
        df.pub_key = df.pub_key.apply(lambda k: k[:8] + "..." + k[-4:])

    # print the dataframe
    click.echo()
    df = df.set_index("pub_key")
    df = df.sort_values(by="time", ascending=False)
    df.time = df.time.apply(lambda t: t.strftime("%Y-%m-%d %H:%M"))
    click.echo(df.to_string())


########################################################################################
# Addresses
########################################################################################


@vaults.command("addresses")
@click.option("--pubkey", help="the vault pub key", required=False)
@click.option("--chains", help="the chains to print", required=False)
def addresses(pubkey, chains):
    """
    Prints the addresses for a vault.
    """
    if pubkey and len(pubkey) == 75:
        vaults = [util.thorchain_vault(pubkey)]
    else:
        vaults = util.thorchain_vaults_asgard()

    if chains:
        chains = chains.upper()
        chains = chains.split(",")

    for v in vaults:
        if pubkey and pubkey not in v["pub_key"]:
            continue

        if chains:
            v["addresses"] = [a for a in v["addresses"] if a["chain"] in chains]
        v["addresses"] = sorted(v["addresses"], key=lambda a: a["chain"])
        for a in v["addresses"]:
            click.echo(f"{v['pub_key'][-4:]}: {a['chain']} => {a['address']}")
        click.echo()


########################################################################################
# Pool Delta
########################################################################################


@vaults.command("pool-delta")
@click.pass_context
def pool_delta(ctx):
    """
    Prints the difference between asgard and pool balances. This should only be used as
    an estimate since more is required for trade assets, outbounds, etc.
    """
    height = ctx.obj["height"] or util.thornode_latest_height()

    vault_balances = collections.defaultdict(int)
    for v in util.thorchain_vaults_asgard(height):
        for coin in v["coins"]:
            vault_balances[coin["asset"]] += int(coin["amount"])

    pools = {}
    for p in util.thorchain_pools(height):
        p["balance_asset"] = int(p["balance_asset"])
        p["balance_rune"] = int(p["balance_rune"])
        pools[p["asset"]] = p

    rows = []
    for asset, amount in vault_balances.items():
        if asset not in pools:
            click.echo(f"skipping asset {asset} with no pool")
            continue

        pool = pools[asset]
        asset_delta = amount - pool["balance_asset"]
        rune_delta = asset_delta * pool["balance_rune"] / pool["balance_asset"]
        rows.append(
            {
                "asset": asset,
                "asset_delta": asset_delta,
                "rune_delta": rune_delta,
            }
        )

    df = pd.DataFrame(rows)
    df = df.set_index("asset")
    df = df.sort_values(by="rune_delta", ascending=False)
    total_rune = df.rune_delta.sum()
    df.asset_delta = df.asset_delta.apply(lambda d: f"{d/1e8:,.2f}")
    df.rune_delta = df.rune_delta.apply(lambda d: f"{d/1e8:,.2f}")

    click.echo()
    click.secho("ESTIMATES ONLY", fg="yellow")
    click.echo(df.to_string())
    click.echo()
    click.echo(f"Total RUNE Delta: {total_rune/1e8:,.2f}")
