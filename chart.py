import datetime
import time

import click
import jq
import tqdm
import pandas as pd
import plotext as plt

import util

########################################################################################
# Commands
########################################################################################


@click.group(cls=util.CustomGroup, help="commands to chart endpoint data in terminal")
def chart():
    pass


########################################################################################
# Value
########################################################################################


@chart.command()
@click.argument("endpoint", type=str)
@click.argument("expression", type=str)
@click.option("--interval", type=int, default=100, help="block interval")
@click.option("--start", type=int, default=-1000, help="start block")
@click.option("--end", type=int, default=-1, help="end block")
def value(endpoint, expression, interval, start, end):
    """
    Chart the expression value over time.
    """
    _chart(endpoint, expression, interval, start, end)


########################################################################################
# Delta
########################################################################################


@chart.command()
@click.argument("endpoint", type=str)
@click.argument("expression", type=str)
@click.option("--interval", type=int, default=100, help="block interval")
@click.option("--start", type=int, default=-1000, help="start block")
@click.option("--end", type=int, default=-1, help="end block")
def delta(endpoint, expression, interval, start, end):
    """
    Chart change in the expression value over time.
    """
    _chart(endpoint, expression, interval, start, end, delta=True)


########################################################################################
# Internal
########################################################################################


def _chart(endpoint, expression, interval, start, end, delta=False):
    # allow reverse index from tip
    latest = util.thornode_latest_height()
    if start < 0:
        start = latest + start
    if end < 0:
        end = latest + end

    # block time for estimated times
    block_time = util.average_block_time()

    expr = jq.compile(expression)
    rows = []

    for height in tqdm.tqdm(range(start, end + 1, interval)):
        data = util.get(endpoint, params={"height": height}).json()

        # evaluate jq expression against data
        values = expr.input(data).all()
        if not values or not values[0] or len(values) > 1:
            click.echo(f"[{height}] expression failed, must return exactly one value")
            return
        value = float(values[0])

        est_block_time = time.time() - (latest - height) * block_time
        dt = datetime.datetime.fromtimestamp(est_block_time)
        fmt_time = dt.strftime("%Y-%m-%d %H:%M")
        rows.append(
            {
                "height": f"[{height}] {fmt_time}",
                "value": value,
            }
        )

    df = pd.DataFrame(rows)
    if delta:
        df["value"] = df["value"].diff().fillna(0)
    plt.simple_bar(df["height"], df["value"])
    plt.show()
