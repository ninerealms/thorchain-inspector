########################################################################################
# Config
########################################################################################

NETWORK = "mainnet"
BLOCK_SECONDS = 6
BLOCKS_PER_YEAR = 86400 * 365.25 / BLOCK_SECONDS
THORNODE_API_ENDPOINT = "https://thornode-archive.ninerealms.com"
THORNODE_RPC_ENDPOINT = "https://rpc-archive.ninerealms.com"
MIDGARD_API_ENDPOINT = "https://midgard.ninerealms.com"
GENESIS_HEIGHT = 1
GENESIS_HEIGHT_UNIX = 1618058210

BINANCE_API = "https://api.binance.org/bc"
BLOCKCYPHER_API = "https://api.blockcypher.com"
BTC_DOT_COM_API = "https://chain.api.btc.com"
DEFILLAMA_API = "https://coins.llama.fi"
DEFILLAMA_CHAIN_MAP = {
    "AVAX": "avax",
    # "BNB": "binancecoin",         # Not supported in the blocks endpoint, but supported in other parts of the DefiLlama API
    # "BCH": "bitcoin-cash",        # Not supported in the blocks endpoint, but supported in other parts of the DefiLlama API
    "ETH": "ethereum",
}

SHAPESHIFT_APIS = {
    "avax": "https://api.avalanche.shapeshift.com",
    "bch": "https://api.bitcoincash.shapeshift.com",
    "btc": "https://api.bitcoin.shapeshift.com",
    "bsc": "https://api.bnbsmartchain.shapeshift.com",
    "doge": "https://api.dogecoin.shapeshift.com",
    "eth": "https://api.ethereum.shapeshift.com",
    "gaia": "https://api.cosmos.shapeshift.com",
    "ltc": "https://api.litecoin.shapeshift.com",
    "thor": "https://api.thorchain.shapeshift.com",
}

THORCHAIN_POOL_MODULE = "thor1g98cy3n9mmjrpn0sxmn63lztelera37n8n67c0"

CHAIN_DECIMALS = {
    "avax": 1e18,
    "bch": 1e8,
    "bnb": 1e8,
    "btc": 1e8,
    "doge": 1e8,
    "eth": 1e18,
    "bsc": 1e18,
    "gaia": 1e6,
    "ltc": 1e8,
    "thor": 1e8,
}

CHAINS = [chain.upper() for chain in CHAIN_DECIMALS.keys() if chain != "thor"]
RAGNAROK_CHAINS = {"TERRA", "BNB"}
