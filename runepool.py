import click
import pandas as pd

import util

########################################################################################
# Commands
########################################################################################


@click.group(cls=util.CustomGroup, help="commands to investigate runepool")
def runepool():
    pass


########################################################################################
# Blocks
########################################################################################


@runepool.command()
@click.option("--height", help="query at the provided height")
def status(height):
    stats = util.thorchain_runepool(height)
    network = util.thorchain_network(height)

    stats['total_pol'] = stats['pol']
    del stats['pol']

    rune_price = int(network["rune_price_in_tor"]) / 1e8
    stats_df = pd.DataFrame.from_dict(stats, orient='index')
    stats_df[['pnl_rune', 'value_rune']] = stats_df[['pnl', 'value']].apply(pd.to_numeric)
    stats_df['pnl_usd'] = stats_df.apply(lambda x: x.pnl_rune * rune_price, axis=1)
    stats_df['value_usd'] = stats_df.apply(lambda x: x.value_rune * rune_price, axis=1)

    stats_df = stats_df[['pnl_rune', 'pnl_usd', 'value_rune', 'value_usd']]
    
    stats_df = stats_df.map(lambda x: "{:,}".format(int(x / 1e8)))
    click.echo(stats_df)