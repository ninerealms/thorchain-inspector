import collections
import concurrent.futures
import math
import re
import time

import click
import pandas as pd
import tqdm
import requests
import retry
from pandarallel import pandarallel

import config
import util


########################################################################################
# Init
########################################################################################

pandarallel.initialize(verbose=0, nb_workers=32)


########################################################################################
# Commands
########################################################################################


@click.group(cls=util.CustomGroup, help="commands to investigate nodes")
def nodes():
    pass


########################################################################################
# Mimir
########################################################################################


@nodes.command()
@click.option("--mimir-key", help="the mimir key to lookup")
@click.option(
    "--match-operator-address", default="", help="regex match operator address"
)
def mimir(mimir_key, match_operator_address):
    """
    Prints node mimir votes for currently active nodes.
    """
    mimirs = util.thornode_mimir_nodes()["mimirs"]
    mimir_nas = collections.defaultdict(lambda: collections.defaultdict(lambda: []))

    nodes = set()
    re_operator_address = re.compile(f".*{match_operator_address}")

    for node in util.thorchain_nodes(util.thornode_latest_height()):

        if node["status"] == "Active" and re_operator_address.match(
            node["node_operator_address"]
        ):

            nodes.add(node["node_address"])

    for m in mimirs:
        if m["signer"] not in nodes:
            continue
        if mimir_key and m["key"] != mimir_key:
            continue
        mimir_nas[m["key"]][m.get("value", 0)].append(m["signer"])

    rows = []
    for key, votes in mimir_nas.items():
        for vote, nas in votes.items():
            rows.append(
                {
                    "mimir": key,
                    "vote": vote,
                    "count": len(nas),
                    "consensus": f"{round(len(nas)/len(nodes)*100, 2)}%",
                }
            )
    print(pd.DataFrame(rows).to_string(index=False))


########################################################################################
# Observation Lag
########################################################################################


@nodes.command()
@click.option("--threshold", default=1, help="the threshold in blocks to filter")
@click.option("--height", help="query at the provided height")
@click.option("--chains", default=[], multiple=True, help="chains to filter")
def observation_lag(threshold, chains, height):
    """
    Prints nodes with observation lag over the provided threshold.
    """
    nodes = util.thorchain_nodes(height)
    rows = []
    for node in nodes:
        if node["status"] != "Active":
            continue
        row = {
            "address": node["node_address"][-4:],
            "operator": node["node_operator_address"][-4:],
        }
        for chain in node.get("observe_chains") or []:
            if chains and chain["chain"] not in chains:
                continue
            row[chain["chain"]] = chain["height"]
        rows.append(row)

    df = pd.DataFrame(rows)
    chain_cols = set(df.columns) - {"address", "operator"}
    for col in chain_cols:
        click.echo(f"Tip Observation ({col}): {df[col].max()}")
        df[col] = df[col].max() - df[col]
    click.echo()

    df = df[(df[list(chain_cols)] > threshold).any(axis=1)]
    df = df.sort_values(by=["operator"])
    click.echo(f"{len(df)} nodes lagging over {threshold} block")
    click.echo(df.to_string(index=False))


########################################################################################
# Bifrost Lag
########################################################################################


@nodes.command()
@click.option(
    "--chains", default=",".join(config.CHAINS), help="comma separated chains to filter"
)
def scanner_lag(chains):
    """
    Prints nodes with scanner lag over the provided threshold.
    """
    bifrost_lag(scanner=True, chains=chains.upper().split(","))


@nodes.command()
@click.option(
    "--chains", default=",".join(config.CHAINS), help="comma separated chains to filter"
)
def daemon_lag(chains):
    """
    Prints nodes with daemon lag over the provided threshold.
    """
    bifrost_lag(scanner=False, chains=chains.upper().split(","))


def bifrost_lag(scanner, chains):
    # fetch node dataframe
    df = util.nodes_dataframe()
    df = df[df.status == "Active"]
    df = df.assign(operator=df.node_operator_address.apply(lambda s: s[-4:]))

    # fetch bifrost scanner status
    def status_scanner(ip):
        @retry.retry(Exception, delay=1, tries=2)
        def fetch():
            return requests.get(f"http://{ip}:6040/status/scanner", timeout=10).json()

        try:
            return fetch()
        except:
            return {}

    # fetch ip address country
    def country(ip):
        try:
            return util.geocode(ip)["countryCode"]
        except:
            return ""

    # parallel apply fetch bifrost scanner status and country
    with concurrent.futures.ThreadPoolExecutor(32) as p:
        fscanner = df.ip_address.apply(lambda ip: p.submit(status_scanner, (ip)))
        fcountry = df.ip_address.apply(lambda ip: p.submit(country, (ip)))
        df = df.assign(
            scanner=fscanner.apply(lambda f: f.result()),
            country=fcountry.apply(lambda f: f.result()),
        )

    # extract scanner and chain height
    df = df[["na", "operator", "scanner", "country"]]
    for chain in chains:
        df = df.assign(
            **{
                chain: df.scanner.apply(
                    lambda s: s.get(chain, {}).get(
                        "block_scanner_height" if scanner else "chain_height"
                    )
                ),
                f"{chain}-tip": df.scanner.apply(
                    lambda s: s.get(chain, {}).get("chain_height")
                ),
            }
        )

    # create summary
    df = df.fillna(0)
    summary_rows = []
    for chain in chains:
        tip = int(df[f"{chain}-tip"].max())
        consensus_diff = tip - int(df[chain].quantile(1 / 3))
        median_diff = tip - int(df[chain].quantile(0.5))
        summary_rows.append(
            {
                "chain": chain,
                "tip": tip,
                "consensus_diff": consensus_diff,
                "median_diff": median_diff,
            }
        )

    # formatting
    unknown = len(df[(df[chains] == 0).all(axis=1)])
    df = df[(df[list(chains)] > 0).any(axis=1)]
    for chain in chains:
        df[chain] = int(df[f"{chain}-tip"].fillna(0).max()) - df[chain].astype(int)
    df = df[["na", "operator", "country"] + chains]

    # sort by operator
    df = df.sort_values(by=["country", "operator"])

    # print all lag
    click.echo(df.to_string(index=False))
    click.echo()
    click.echo(f"{unknown} unreachable nodes")
    click.echo()

    # print summary
    click.echo(pd.DataFrame(summary_rows).to_string(index=False))


########################################################################################
# Dataframe
########################################################################################


@nodes.command("dataframe")
@click.argument("query")
@click.option("--location/--no-location", default=False, help="add location column")
@click.option("--provider/--no-provider", default=False, help="add provider column")
@click.option("--shard/--no-shard", default=False, help="add asgard shard column")
@click.option("--p2pid/--no-p2pid", default=False, help="p2pid column")
@click.option("--balance/--no-balance", default=False, help="add balance column")
@click.option("--mimir", default="", help="add mimir column")
@click.option("--height", help="query at the provided height")
def ndf(query, location, provider, shard, p2pid, balance, mimir, height):
    """
    Prints a dataframe returned by the provided expression with variable "df".

    Example: tci nodes dataframe 'df[df.status=="Active"][["na", "ip_address"]]'
    """

    df = util.nodes_dataframe(height)

    if location:

        def geocode_str(ip):
            try:
                res = util.geocode(ip)
                return f"{res['city']}, {res['countryCode']}"
            except:
                return ""

        with concurrent.futures.ThreadPoolExecutor(32) as p:
            futures = df.ip_address.apply(lambda ip: p.submit(geocode_str, (ip)))
            df = df.assign(location=futures.apply(lambda f: f.result()))

    if provider:
        df = df.assign(provider=df.ip_address.apply(lambda ip: util.pretty_whois(ip)))

    if balance:
        df = df.assign(
            balance=df.node_address.apply(
                lambda na: util.thorchain_address_balance_rune(na) if na else None
            )
        )

    if shard:
        nodes = util.thorchain_nodes(util.thornode_latest_height())
        pk_na = {}
        for node in nodes:
            if node["status"] != "Active":
                continue
            if "pub_key_set" in node:
                pk_na[node["pub_key_set"]["secp256k1"]] = node["node_address"]

        asgard = util.thorchain_vaults_asgard(height)
        na_shard = {}
        for idx, a in enumerate(asgard):
            for pk in a["membership"]:
                if pk not in pk_na:
                    continue
                na_shard[pk_na[pk]] = idx
        df = df.assign(shard=df.node_address.apply(lambda a: na_shard.get(a, -1)))

    if mimir:
        na_mimirs = {}
        for m in util.thornode_mimir_nodes()["mimirs"]:
            if m["key"] != mimir:
                continue
            na_mimirs[m["signer"]] = m.get("value", 0)
        df = df.assign(mimir=df.node_address.apply(lambda na: na_mimirs.get(na)))

    if p2pid:
        click.echo("gathering p2p ids...")
        df = df.assign(p2pid=df.ip_address.apply(util.p2pid))
        click.echo("gathered p2p ids")

    res = eval(query)

    if type(res) is pd.DataFrame:
        click.echo(res.to_string())
    else:
        click.echo(res)


########################################################################################
# Watch Sync
########################################################################################


@nodes.command("watch-sync")
@click.argument("node")
@click.option("--tip", default=0)
def watch_sync(node, tip):
    """
    Watches the sync state of the node at the provided Tendermint RPC endpoint.
    """
    if tip == 0:
        tip = util.thornode_latest_height()

    height = lambda: int(
        util.get(f"{node}/status").json()["result"]["sync_info"]["latest_block_height"]
    )
    last = height()

    with tqdm.tqdm(initial=last, total=tip, unit="block", smoothing=0.01) as bar:
        while True:
            time.sleep(1)
            h = height()
            bar.update(h - last)
            last = h


########################################################################################
# Slash Diff
########################################################################################


@nodes.command("slash-diff")
@click.option("--blocks", default=655, help="the number of blocks to diff")  # ~ 1 hour
@click.option("--location/--no-location", default=False, help="add location column")
@click.option("--height", default=0, help="query at the provided height")
def slash_diff(blocks, location, height):
    """
    Prints the difference in slash points for all active nodes over provided blocks.
    """
    height = height or util.thornode_latest_height()
    current = util.thorchain_nodes(height)
    previous = util.thorchain_nodes(height - blocks)

    rows = []
    for c in current:
        if c["status"] != "Active":
            continue
        for p in previous:
            if c["node_address"] == p["node_address"]:
                rows.append(
                    {
                        "address": c["node_address"],
                        "operator": c["node_operator_address"],
                        "ip_address": c["ip_address"],
                        "points": c["slash_points"] - p["slash_points"],
                        "bond_slash": int(c["total_bond"]) - int(p["total_bond"]),
                        "before": p["slash_points"],
                        "after": c["slash_points"],
                    }
                )

    df = pd.DataFrame(rows)

    if location:
        df = df.assign(location=df.ip_address.apply(lambda ip: str(geocoder.ip(ip)[0])))

    # display
    df.address = df.address.apply(lambda s: s[:4] + "..." + s[-4:])
    df.operator = df.operator.apply(lambda s: s[-4:])
    df.sort_values("points", inplace=True)
    df.set_index("address", inplace=True)
    print(df.to_string())
    print()
    print(
        f"total => {df.points.sum()} points, {df.bond_slash.sum()/1e8:,.8f} RUNE bond"
    )


########################################################################################
# Bond Diff
########################################################################################


@nodes.command("bond-diff")
@click.option("--heights", type=str, help="comma separated list of heights")
@click.option("--operator/--no-operator", default=False, help="group by operator")
@click.option("--node/--no-node", default=False, help="group by node")
@click.option("--provider/--no-provider", default=False, help="group by provider")
@click.option("--short/--long", default=True, help="shorten addresses")
def bond_diff(heights, operator, node, provider, short):
    """
    Prints aggregate difference in bond for nodes at provided heights.
    """
    if all([not operator, not node, not provider]):
        operator = True
        node = True
        provider = True

    rows = []
    for height in tqdm.tqdm(heights.split(",")):
        height = int(height)
        current_nodes = util.thorchain_nodes(height)
        previous_nodes = util.thorchain_nodes(height - 1)

        for n in current_nodes:
            for bp in (n["bond_providers"] or {}).get("providers") or []:
                rows.append(
                    {
                        "operator": n["node_operator_address"],
                        "node": n["node_address"],
                        "provider": bp["bond_address"],
                        "bond": int(bp["bond"]),
                    },
                )

        for n in previous_nodes:
            for bp in (n["bond_providers"] or {}).get("providers") or []:
                rows.append(
                    {
                        "operator": n["node_operator_address"],
                        "node": n["node_address"],
                        "provider": bp["bond_address"],
                        "bond": -int(bp["bond"]),
                    },
                )

    df = pd.DataFrame(rows)

    # shorten addresses
    if short:
        df.operator = df.operator.apply(lambda s: s[:4] + "..." + s[-4:])
        df.node = df.node.apply(lambda s: s[:4] + "..." + s[-4:])
        df.provider = df.provider.apply(lambda s: s[:4] + "..." + s[-4:])

    # group by the provided columns
    group = []
    if operator:
        group.append("operator")
    else:
        df.drop("operator", axis=1, inplace=True)
    if node:
        group.append("node")
    else:
        df.drop("node", axis=1, inplace=True)
    if provider:
        group.append("provider")
    else:
        df.drop("provider", axis=1, inplace=True)
    df = df.groupby(group).sum().reset_index()

    # skip zero diffs
    df = df[df.bond != 0]
    df.set_index(group, inplace=True)

    # output totals
    click.echo(df.to_string())
    click.echo(f"total diff => {df.bond.sum()/1e8:,.8f}")
    click.echo(f"total bond change nodes => {len(df)}")


########################################################################################
# Missing-Signers
########################################################################################


@nodes.command("missing-signers")
@click.option("--height", type=int, help="query at the provided height")
@click.option("--blocks", default=10, help="the number of past blocks to check")
def missing_signers(height, blocks):
    """
    Print nodes not signing blocks.
    """
    height = height or (util.thornode_latest_height() - 5)  # 5 blocks ago by default

    nodes = util.thorchain_nodes(height)
    df = pd.DataFrame(nodes)
    df = df[df.status == "Active"]
    df = df.assign(
        cons_addr=df.validator_cons_pub_key.apply(
            lambda a: util.pubkey_address(a, True).upper()
        ),
    )
    df.node_address = df.node_address.apply(lambda na: na[-4:])
    df.node_operator_address = df.node_operator_address.apply(lambda na: na[-4:])

    # collect count of missed signers
    missed = collections.defaultdict(int)

    def _missed(na):
        missed[na] += 1

    # check block range
    for height in tqdm.tqdm(range(height, height - blocks, -1)):
        block = util.get(
            f"{config.THORNODE_RPC_ENDPOINT}/block", params={"height": height}
        ).json()
        signatures = block["result"]["block"]["last_commit"]["signatures"]
        signers = {s["validator_address"] for s in signatures}

        # print nodes that did not sign
        unsigned = lambda a: not a or a not in signers
        df[df.cons_addr.apply(unsigned)]["node_address"].apply(_missed)

    # display
    mdf = pd.DataFrame.from_dict(missed, orient="index", columns=["missed"])
    mdf = mdf.merge(df, left_index=True, right_on="node_address")
    mdf = mdf.sort_values("missed", ascending=False)
    mdf = mdf.set_index("node_address")
    print(mdf[["node_operator_address", "missed"]].to_string())


########################################################################################
# Lagging
########################################################################################


@nodes.command("lagging")
@click.option("--height", default=0, help="query at the provided height")
@click.option("--threshold", default=1, help="the threshold in blocks to filter")
def lagging(height, threshold):
    """
    Print nodes lagging behind the latest thornode block.
    """
    height = height or util.thornode_latest_height()
    nodes = util.thorchain_nodes(height)
    df = pd.DataFrame(nodes)
    df = df[df.status == "Active"]
    df = df.assign(
        height=df.ip_address.parallel_apply(
            lambda ip: util.status(ip).get("sync_info", {}).get("latest_block_height")
        )
    )

    if len(df[df.height.isnull()]) > 0:
        print(f"{len(df[df.height.isnull()])} nodes with unreachable rpc")

    df = df[df.height.notnull()]
    df = df.assign(lag=height - df.height.astype(int))
    df.node_address = df.node_address.apply(lambda na: na[-4:])
    df.node_operator_address = df.node_operator_address.apply(lambda na: na[-4:])

    # print nodes that did not sign
    df = df[df.lag > threshold]
    if len(df) == 0:
        print("no lagging nodes detected")
        return
    df = df[["node_address", "node_operator_address", "lag"]]
    df.sort_values("lag", inplace=True)
    print(df.to_string(index=False))


########################################################################################
# Peer Addresses
########################################################################################


@nodes.command("peer-addresses")
@click.option("--height", default=0, help="query at the provided height")
@click.option("--active", is_flag=True, help="filter by active nodes")
@click.option("--ready", is_flag=True, help="filter by active and ready nodes")
def peer_addresses(height, active, ready):
    """
    Print node peer addresses.
    """
    height = height or util.thornode_latest_height()
    nodes = util.thorchain_nodes(height)
    df = pd.DataFrame(nodes)
    if ready:
        df["preflight_status"] = df.preflight_status.get("status")
        df = df[(df.status == "Active") | (df.preflight_status == "Ready")]
    elif active:
        df = df[df.status == "Active"]
    df = df.assign(p2pid=df.ip_address.parallel_apply(lambda ip: util.p2pid(ip)))

    # format and print
    df.node_address = df.node_address.apply(lambda na: na[-4:])
    df.node_operator_address = df.node_operator_address.apply(lambda na: na[-4:])
    df = df[
        [
            "status",
            "node_address",
            "node_operator_address",
            "version",
            "p2pid",
            "ip_address",
        ]
    ]
    df.sort_values("node_operator_address", inplace=True)
    print(df.to_string(index=False))


########################################################################################
# Failing Keygen
########################################################################################


@nodes.command("failing-keygen")
@click.option("--start", type=int, help="start block to scan")
@click.option("--blocks", default=720, help="number of blocks to scan")
def failing_keygen(start, blocks):
    """
    Print nodes that have not succeeded in any keygen over the provided blocks.
    """
    # optional dependency required for this command
    try:
        import cosmoscan
    except ImportError:
        click.secho("please install cosmoscan", fg="red")
        return

    df = util.nodes_dataframe()
    df.node_operator_address = df.node_operator_address.apply(lambda na: na[-4:])

    # use cosmoscan to find keygen messages within the last churn try
    click.echo("scanning for keygen messages...")
    vaults = set()
    tss_pool_successes = set()
    cosmoscan.scan(
        cosmoscan.tx_events_listener(
            lambda h, txh, tx, txr, e: vaults.add(e["pubkey"]), types={"tss_keygen"}
        ),
        cosmoscan.tx_msgs_listener(
            lambda h, txh, m: tss_pool_successes.add(m["signer"]),
            types={"MsgTssPool"},
        ),
        start=start or -blocks,
        end=-1,
        progress=True,
    )

    # get all vault members
    members = []
    for pubkey in vaults:
        v = util.thorchain_vault(pubkey)
        members.extend(v["membership"])

    # only active and ready nodes
    df = df[df.status.isin(["Active", "Ready"])]
    df = df[df.leave_height == 0]

    # filter to nodes that are not in new vaults
    df = df[~df["pub_key_set.secp256k1"].isin(members)]

    # format and print nodes in unsuccessful keygens
    columns = [
        "status",
        "na",
        "node_operator_address",
        "version",
    ]
    click.secho("\n\nFailed Keygens:", fg="red")
    df2 = df[columns]
    df2 = df2.sort_values("node_operator_address")
    print(df2.to_string(index=False))

    # print nodes that have not sent a successful MsgTssPool
    click.secho("\nFailed Keygens, Missing MsgTssPool:", fg="red")
    df2 = df[~df.node_address.isin(tss_pool_successes)]
    df2 = df2[columns]
    df2 = df2.sort_values("node_operator_address")
    print(df2.to_string(index=False))

    # print nodes that have sent a successful MsgTssPool
    click.secho("\nFailed Keygens, Successful MsgTssPool:", fg="green")
    df2 = df[df.node_address.isin(tss_pool_successes)]
    df2 = df2[columns]
    df2 = df2.sort_values("node_operator_address")
    print(df2.to_string(index=False))


########################################################################################
# APR
########################################################################################


@nodes.command("apr")
@click.option("--churns", default=3, help="number of churns to compute APR")
@click.option("--height", help="the ending height")
def apr(churns, height):
    """
    Print node APR for recent churns.
    """
    if height:  # fetch one extra if last is not current so we have the start height
        churns += 1

    churn_heights = util.churn_heights(churns, height)
    churn_heights.reverse()

    # if no height provided, compute for current churn with partial data
    if not height:
        churn_heights.append(util.thornode_latest_height())

    for i, churn_height in enumerate(churn_heights[1:]):
        previous_height = churn_heights[i]
        df = util.nodes_dataframe(churn_height - 1, mimirs=False)
        reward = df[df.status == "Active"].current_award.astype(int).sum()

        # scale to disregard unpaid slash reward from previous churn
        scale = 1 - int(
            util.thorchain_network(previous_height)["bond_reward_rune"]
        ) / int(util.thorchain_network(churn_height - 1)["bond_reward_rune"])
        normalized_reward = reward * scale

        # compute apr
        bond = df[df.status == "Active"].total_bond.astype(int).sum()
        churn_blocks = churn_height - churn_heights[i]
        apr = normalized_reward / bond * config.BLOCKS_PER_YEAR / churn_blocks

        # tweak text for current partial churn
        if not height and i == len(churn_heights) - 2:
            churn_height = "current"
            churn_blocks = util.thornode_mimir().get("CHURNINTERVAL", 43200)

        # calculate bonus apr based on carryover rewards from previous churn slash
        bonus = reward * (1 - scale) / bond * config.BLOCKS_PER_YEAR / churn_blocks

        prefix = f"[{churn_height}]"
        click.echo(f"{prefix:10} APR:{apr:.2%} + Bonus:{bonus:.2%}")


########################################################################################
# Redline
########################################################################################


@nodes.command("redline")
@click.option("--height", help="query at the provided height")
def redline(height):
    """
    Print the redline slash points.
    """
    height = height or util.thornode_latest_height()
    df = util.nodes_dataframe(height, mimirs=False)

    # fetch redline config
    mimir = util.thornode_mimir(height)
    redline = mimir.get("BADVALIDATORREDLINE")

    # filter active nodes and find the mean score
    df = df[df.status == "Active"]
    mean_slash = df.slash_points.mean()
    max_slash = df.slash_points.max()
    click.secho(f"Redline Slash: {int(mean_slash*redline)}", fg="red")
    click.echo(f"    Max Slash: {int(max_slash)}")
    click.echo(f"   Mean Slash: {int(mean_slash)}")


########################################################################################
# Bond
########################################################################################


@nodes.command("bond")
@click.option("--height", help="query at the provided height")
def bond(height):
    """
    Print bond stats.
    """
    height = height or util.thornode_latest_height()
    df = util.nodes_dataframe(height, mimirs=False)

    # filter active nodes and find the mean score
    df = df[df.status == "Active"]
    bonds = df.total_bond.astype(int)
    bonds = bonds.sort_values()
    total_bond = bonds.sum()
    effective_bond = bonds.quantile(0.66)
    total_effective_bond = bonds.apply(lambda b: min(b, effective_bond)).sum()
    security_bond = bonds[: math.ceil(len(bonds) * 2 / 3)].sum()

    click.echo(f"    Total Bond: {total_bond/1e8:,.8f} RUNE")
    click.echo(f"Effective Bond: {total_effective_bond/1e8:,.8f} RUNE")
    click.echo(f" Security Bond: {security_bond/1e8:,.8f} RUNE")


########################################################################################
# Provider Rewards
########################################################################################


@nodes.command("provider-rewards")
@click.option("--churns", default=3, help="number of churns to compute rewards")
@click.argument("provider")
def provider_rewards(churns, provider):
    """
    Print rewards for the given provider.
    """
    # find churn heights
    churn_heights = util.churn_heights(churns)

    # find the bonds at the churn height and block prior
    rows = []
    for churn_height in tqdm.tqdm(churn_heights):
        total_before = 0
        total_after = 0
        nodes_before = util.thorchain_nodes(churn_height - 1)
        nodes_after = util.thorchain_nodes(churn_height)

        for node in nodes_before:
            for bp in (node["bond_providers"] or {}).get("providers") or []:
                if bp["bond_address"] == provider:
                    total_before += int(bp["bond"])

        for node in nodes_after:
            for bp in (node["bond_providers"] or {}).get("providers") or []:
                if bp["bond_address"] == provider:
                    total_after += int(bp["bond"])

        network = util.thorchain_network(churn_height)
        rewards = total_after - total_before
        usd_value = rewards * int(network["rune_price_in_tor"]) / 1e8
        date = util.thorchain_height_to_date(churn_height).strftime("%Y-%m-%d")

        rows.append(
            {
                "date": date,
                "height": churn_height,
                "before": total_before / 1e8,
                "after": total_after / 1e8,
                "rewards": rewards / 1e8,
                "usd_value": usd_value / 1e8,
                "rune_price": int(network["rune_price_in_tor"]) / 1e8,
            }
        )

    # order by date ascending
    rows = reversed(rows)
    df = pd.DataFrame(rows)

    # compute totals
    bond_change = df.before[1:].sum() - df.after[:-1].sum()
    total_rewards = df.rewards.sum()
    total_usd_rewards = df.usd_value.sum()

    # format
    # df = df.drop(columns=["before", "after"])
    df["rewards"] = (df["rewards"]).apply(lambda x: f"{x:,.8f}")
    df["usd_value"] = (df["usd_value"]).apply(lambda x: f"${x:,.2f}")
    df["rune_price"] = df["rune_price"].apply(lambda x: f"${x:,.2f}")
    click.echo(df.to_string(index=False))

    click.echo()
    click.echo(f"Total RUNE Rewards: {total_rewards:,.8f}")
    click.echo(f"Total USD Rewards: {total_usd_rewards:,.2f}")
    click.echo(f"Bond Change (bond/unbond/slash): {bond_change:,.8f}")
