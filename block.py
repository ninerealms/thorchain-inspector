import base64
import click
import json
import pytz

import config

from dateutil import parser
from datetime import datetime
from util import thorchain_block, average_block_time, thornode_latest_height, get
import util


########################################################################################
# Commands
########################################################################################


@click.group(cls=util.CustomGroup, help="commands to investigate blocks")
def block():
    pass


########################################################################################
# Blocks
########################################################################################


@block.command()
@click.argument("height", type=int)
def events(height):
    """
    Print end blocks events for the provided block.
    """
    res = get(
        f"{config.THORNODE_RPC_ENDPOINT}/block_results",
        params={"height": height},
    )
    res = res.json()
    events = (res["result"]["begin_block_events"] or []) + (
        res["result"]["end_block_events"] or []
    )
    for e in events:
        em = {}
        for k, v in e.items():
            if k == "attributes":
                for a in v:
                    try:
                        em[base64.b64decode(a["key"]).decode("utf-8")] = (
                            base64.b64decode(a["value"]).decode("utf-8")
                        )
                    except:
                        pass
            else:
                em[k] = v

        print(json.dumps(em, indent=2))


@block.command()
@click.argument("ts", type=int)
@click.option("--tz", default=None, help="timezone to display")
def until_time(ts, tz):
    """
    Print approximate block number at provided unix timestamp.
    """
    block_avg = average_block_time(20000)
    current_block = thorchain_block(thornode_latest_height())
    current_block_time = parser.isoparse(
        current_block["result"]["block"]["header"]["time"]
    )

    blocks = float(ts - current_block_time.timestamp()) / block_avg
    thorchain_height = int(current_block["result"]["block"]["header"]["height"]) + int(
        blocks
    )

    # timezone from tz string
    if tz:
        tz = pytz.timezone(tz)

    time_string = (
        datetime.fromtimestamp(ts).astimezone(tz).strftime("%Y-%m-%d %H:%M %Z")
    )

    print(
        f"Approximately {int(blocks)} blocks until {time_string} at THORChain height {thorchain_height}"
    )


@block.command()
@click.argument("height", type=int)
@click.option("--tz", default=None, help="timezone to display")
def until_height(height, tz):
    """
    Print approximate block time at THORChain height.
    """
    block_avg = average_block_time()
    current_block = thorchain_block(thornode_latest_height())
    current_block_time = parser.isoparse(
        current_block["result"]["block"]["header"]["time"]
    )

    blocks_diff = height - int(current_block["result"]["block"]["header"]["height"])
    t = int(current_block_time.timestamp() + (blocks_diff * block_avg))

    # timezone from tz string
    if tz:
        tz = pytz.timezone(tz)

    time_string = datetime.fromtimestamp(t).astimezone(tz).strftime("%Y-%m-%d %H:%M %Z")

    print(
        f"Approximately {int(blocks_diff)} blocks until {time_string} at THORChain height {height}"
    )
