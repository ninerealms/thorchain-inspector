import click

import bech32_util as bech32
import util
from util import thorchain_address_balance_rune

########################################################################################
# Commands
########################################################################################


@click.group(cls=util.CustomGroup, help="commands to investigate addresses")
def address():
    pass


########################################################################################
# Address
########################################################################################


@address.command("ed25519-pubkey")
@click.argument("pubkey", type=str)
def ed25519_pubkey(pubkey):
    """
    Converts an ED25519 pubkey to an address.
    """
    click.echo(f"{util.pubkey_address(pubkey, True)}: {util.pubkey_address(pubkey)}")


@address.command("convert-prefix")
@click.argument("prefix", type=str)
@click.argument("address", type=str)
def convert_prefix(prefix, address):
    """
    Converts the bech32 prefix of an address.
    """
    _, data, _ = bech32.bech32_decode(address)
    click.echo(bech32.bech32_encode(prefix, data, 1))


@address.command("balance")
@click.argument("address", type=str)
def balance(address):
    """
    Prints RUNE Balance of address
    """
    print(thorchain_address_balance_rune(address))
