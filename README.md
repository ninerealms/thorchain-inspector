# Thorchain Inspector

The `tci` command provides convenience utilities for investigating Thorchain network state.

# Installation

Install the Python package in editable mode to allow local changes to take immediate effect.

```bash
pip3 install -e .
```
