import click

import util

import tqdm


########################################################################################
# Commands
########################################################################################


@click.group(cls=util.CustomGroup, help="commands to investigate invariants")
def invariants():
    pass


########################################################################################
# Check
########################################################################################


@invariants.command("check")
def check():
    """
    Check all invariants.
    """
    for invariant in util.thorchain_invariants()["invariants"]:
        broken = util.thorchain_invariant(invariant)["broken"]
        if broken:
            click.secho(f"{invariant} broken", fg="red")
        else:
            click.secho(f"{invariant} ok", fg="green")


########################################################################################
# Find Break Height
########################################################################################


@invariants.command("find-break-height")
@click.argument("invariant")
@click.option("--start", type=int, default=None, help="start height")
@click.option("--end", type=int, default=None, help="end height")
def find_break_height(invariant, start, end):
    """
    Finds the first height where an invariant breaks.
    """
    if not end:
        end = util.thornode_latest_height()

    if not start:
        broken = False
        pbar = tqdm.tqdm(range(end, end - 10000, -1))
        for height in pbar:
            if util.thorchain_invariant(invariant, height)["broken"]:
                if not broken:
                    click.secho(f"break at height {height}", fg="red")
                broken = True
            elif broken:
                pbar.disable = True
                click.secho(f"first break at height {height+1}", fg="red")
                return
    else:
        pbar = tqdm.tqdm(range(start, end))
        for height in pbar:
            if util.thorchain_invariant(invariant, height)["broken"]:
                pbar.disable = True
                click.secho(f"break at height {height}", fg="red")
                return

    print("no break found")
