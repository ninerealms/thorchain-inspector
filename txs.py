import base64
import collections
import json

import click

import config
import util

########################################################################################
# Commands
########################################################################################


@click.group(cls=util.CustomGroup, help="commands to investigate txs")
def txs():
    pass


########################################################################################
# Events
########################################################################################


@txs.command()
@click.argument("tx", type=str)
@click.option(
    "--full", required=False, default=False, type=bool, help="get full tx output"
)
def events(tx, full):
    """
    Print tx event for provided hash.

    tx: tx hash
    full: print full tx event
    """
    res = util.get(f"{config.THORNODE_API_ENDPOINT}/cosmos/tx/v1beta1/txs/{tx}")

    if not full:
        for e in res.json()["tx_response"]["events"]:
            em = {}
            for k, v in e.items():
                if k == "attributes":
                    for a in v:
                        try:
                            em[base64.b64decode(a["key"]).decode("utf-8")] = (
                                base64.b64decode(a["value"]).decode("utf-8")
                            )
                        except:
                            pass
                else:
                    em[k] = v

            print(json.dumps(em, indent=2))
    else:
        print(json.dumps(res.json(), indent=2))


########################################################################################
# Events
########################################################################################


@txs.command()
@click.argument("tx", type=str)
def observers(tx):
    """
    Print tx observers for provided hash.

    tx: tx hash
    """
    details = util.get(
        f"{config.THORNODE_API_ENDPOINT}/thorchain/tx/details/{tx}"
    ).json()
    nodes = util.thorchain_nodes(details.get("consensus_height"))
    operators = {n["node_address"]: n["node_operator_address"][-4:] for n in nodes}
    nodes = list(filter(lambda n: n["status"] == "Active", nodes))
    node_observation_count = collections.defaultdict(int)

    for tx in details["txs"]:
        print(f"Observed Height: {tx['external_observed_height']}")
        print(f"Confirmation Delay Height: {tx['external_confirmation_delay_height']}")
        coin_amount = tx["tx"]["coins"][0]["amount"]
        coin_asset = tx["tx"]["coins"][0]["asset"]
        print(f"Coins: {coin_amount} {coin_asset}")
        gas_amount = tx["tx"]["gas"][0]["amount"]
        gas_asset = tx["tx"]["gas"][0]["asset"]
        print(f"Gas: {gas_amount} {gas_asset}")
        for signer in tx["signers"]:
            node_observation_count[signer] += 1
            print(f"Signer: {signer} {operators.get(signer, 'unknown')}")
        print()

    observed_nodes = len(node_observation_count)
    total_nodes = len(nodes)
    click.echo(f"{observed_nodes}/{total_nodes} nodes observed")

    for node in nodes:
        node_address = node["node_address"]
        if node_address not in node_observation_count:
            operator = operators[node_address]
            click.echo(click.style("Missed: ", fg="red") + f"{node_address} {operator}")
