import datetime
import functools
import hashlib
from dateutil import parser
from typing import Optional, Set, Dict

import click
import ipwhois
import pandas as pd
import requests
import retry

import bech32_util as bech32
import config

########################################################################################
# Requests
########################################################################################


def get(*args, **kwargs):
    # set client id header to self identify
    headers = kwargs.setdefault("headers", {})
    headers["X-Client-ID"] = "tci"
    return requests.get(*args, **kwargs)


# ------------------------------ Thornode ------------------------------


@functools.cache
@retry.retry(Exception, delay=1, backoff=2, tries=3)
def thornode_latest_height():
    res = get(f"{config.THORNODE_RPC_ENDPOINT}/status").json()
    return (
        int(res["result"]["sync_info"]["latest_block_height"]) - 2
    )  # -2 to avoid race on endpoints


@functools.cache
@retry.retry(Exception, delay=1, backoff=2, tries=3)
def thornode_mimir(height: Optional[int] = None, default: Optional[any] = None):
    params = {}
    if height:
        params["height"] = height
    res = get(f"{config.THORNODE_API_ENDPOINT}/thorchain/mimir", params=params).json()
    if "error" in res:
        raise Exception(res["error"])

    if res is None and default is not None:
        return default

    return res


@functools.cache
@retry.retry(Exception, delay=1, backoff=2, tries=3)
def thornode_mimir_nodes():
    res = get(f"{config.THORNODE_API_ENDPOINT}/thorchain/mimir/nodes_all").json()
    if "error" in res:
        raise Exception(res["error"])
    return res


@functools.cache
@retry.retry(Exception, delay=1, backoff=2, tries=3)
def thorchain_pools(height: Optional[int] = None):
    params = {}
    if height:
        params["height"] = height
    res = get(f"{config.THORNODE_API_ENDPOINT}/thorchain/pools", params=params).json()
    if "error" in res:
        raise Exception(res["error"])
    return res


@functools.cache
@retry.retry(Exception, delay=1, backoff=2, tries=3)
def thorchain_nodes(height: Optional[int] = None):
    params = {}
    if height:
        params["height"] = height
    res = get(f"{config.THORNODE_API_ENDPOINT}/thorchain/nodes", params=params).json()
    if "error" in res:
        raise Exception(res["error"])
    return res


@functools.cache
@retry.retry(Exception, delay=1, backoff=2, tries=3)
def thorchain_network(height: Optional[int] = None):
    params = {}
    if height:
        params["height"] = height
    res = get(f"{config.THORNODE_API_ENDPOINT}/thorchain/network", params=params).json()
    if "error" in res:
        raise Exception(res["error"])
    return res


@functools.cache
@retry.retry(Exception, delay=1, backoff=2, tries=3)
def thorchain_lastblock(height: Optional[int] = None):
    params = {}
    if height:
        params["height"] = height
    res = get(
        f"{config.THORNODE_API_ENDPOINT}/thorchain/lastblock", params=params
    ).json()
    if "error" in res:
        raise Exception(res["error"])
    return res


@functools.cache
@retry.retry(Exception, delay=1, backoff=2, tries=3)
def thorchain_inbound_addresses(height: Optional[int] = None):
    params = {}
    if height:
        params["height"] = height
    res = get(
        f"{config.THORNODE_API_ENDPOINT}/thorchain/inbound_addresses", params=params
    ).json()
    if "error" in res:
        raise Exception(res["error"])
    return res


@functools.cache
@retry.retry(Exception, delay=1, backoff=2, tries=3)
def thorchain_block(height: Optional[int] = None):
    params = {}
    if height:
        params["height"] = height
    return get(f"{config.THORNODE_RPC_ENDPOINT}/block", params=params).json()


def thorchain_height_to_date(height: Optional[int] = None) -> datetime.datetime:
    return parser.isoparse(thorchain_block(height)["result"]["block"]["header"]["time"])


@functools.cache
@retry.retry(Exception, delay=1, backoff=2, tries=3)
def average_block_time(sample: int = 5000):
    current_block = thorchain_block()
    current_block_height = current_block["result"]["block"]["header"]["height"]
    current_block_time = parser.isoparse(
        current_block["result"]["block"]["header"]["time"]
    )

    past_block_time = thorchain_height_to_date((int(current_block_height) - sample))

    return float(current_block_time.timestamp() - past_block_time.timestamp()) / float(
        sample
    )


@functools.cache
@retry.retry(Exception, delay=1, backoff=2, tries=3)
def thorchain_vaults_asgard(height: Optional[int] = None):
    params = {}
    if height:
        params["height"] = height
    res = get(
        f"{config.THORNODE_API_ENDPOINT}/thorchain/vaults/asgard", params=params
    ).json()
    if "error" in res:
        raise Exception(res["error"])
    return res


@functools.cache
@retry.retry(Exception, delay=1, backoff=2, tries=3)
def thorchain_vault(pub_key: str, height: Optional[int] = None):
    params = {}
    if height:
        params["height"] = height
    res = get(
        f"{config.THORNODE_API_ENDPOINT}/thorchain/vault/{pub_key}", params=params
    ).json()
    if "error" in res:
        raise Exception(res["error"])
    return res


@functools.cache
@retry.retry(Exception, delay=1, backoff=2, tries=3)
def thorchain_vaults_yggdrasil(height: Optional[int] = None):
    params = {}
    if height:
        params["height"] = height
    res = get(
        f"{config.THORNODE_API_ENDPOINT}/thorchain/vaults/yggdrasil",
        params=params,
    ).json()
    if "error" in res:
        raise Exception(res["error"])
    return res


@retry.retry(Exception, delay=1, backoff=2, tries=3)
def thorchain_address_balance_rune(address: str):
    res = get(
        f"{config.THORNODE_API_ENDPOINT}/cosmos/bank/v1beta1/balances/{address}"
    ).json()
    for coin in res["balances"]:
        if coin["denom"] == "rune":
            return int(coin["amount"]) / 1e8


@retry.retry(Exception, delay=1, backoff=2, tries=3)
def thorchain_module_balance_rune(module: str, height: Optional[int] = None) -> float:
    res = get(
        f"{config.THORNODE_API_ENDPOINT}/thorchain/balance/module/{module}",
        params={"height": height} if height else {},
    ).json()
    for coin in res["coins"]:
        if coin["denom"] == "rune":
            return int(coin["amount"]) / 1e8
    return 0


@retry.retry(Exception, delay=1, backoff=2, tries=3)
def thorchain_invariants(height: Optional[int] = None):
    params = {}
    if height:
        params["height"] = height
    res = get(
        f"{config.THORNODE_API_ENDPOINT}/thorchain/invariants",
        params=params,
    ).json()
    if "error" in res:
        raise Exception(res["error"])
    return res


@retry.retry(Exception, delay=1, backoff=2, tries=3)
def thorchain_invariant(name: str, height: Optional[int] = None):
    params = {}
    if height:
        params["height"] = height
    res = get(
        f"{config.THORNODE_API_ENDPOINT}/thorchain/invariant/{name}",
        params=params,
    ).json()
    if "error" in res:
        raise Exception(res["error"])
    return res


@retry.retry(Exception, delay=1, backoff=2, tries=3)
def thorchain_runepool(height: Optional[int] = None):
    params = {}
    if height:
        params["height"] = height
    res = get(
        f"{config.THORNODE_API_ENDPOINT}/thorchain/runepool",
        params=params,
    ).json()
    if "error" in res:
        raise Exception(res["error"])
    return res


@retry.retry(Exception, delay=1, backoff=2, tries=3)
def thorchain_constants(height: Optional[int] = None):
    params = {}
    if height:
        params["height"] = height
    res = get(
        f"{config.THORNODE_API_ENDPOINT}/thorchain/constants",
        params=params,
    ).json()
    if "error" in res:
        raise Exception(res["error"])
    return res


# ------------------------------ Midgard ------------------------------


def midgard_nodes():
    return get(f"{config.MIDGARD_API_ENDPOINT}/v2/nodes").json()


def midgard_pools():
    return get(f"{config.MIDGARD_API_ENDPOINT}/v2/pools").json()


def midgard_network():
    return get(f"{config.MIDGARD_API_ENDPOINT}/v2/network").json()


@functools.cache
def status(ip: str):
    try:
        return get(f"http://{ip}:27147/status", timeout=2).json()["result"]
    except:
        return {}


@functools.cache
def p2pid(ip: str):
    try:
        res = requests.get(f"http://{ip}:6040/p2pid", timeout=2)
        if res.status_code == 200:
            return res.text
        return f"Status: {res.status_code}"
    except:
        pass
    return "unknown"


########################################################################################
# Balances
########################################################################################


@retry.retry(Exception, delay=1, backoff=2, tries=3)
def get_balance(chain: str, address: str):
    chain = chain.lower()

    if chain in config.SHAPESHIFT_APIS:
        endpoint = f"{config.SHAPESHIFT_APIS[chain]}/api/v1/account/{address}"
        res = get(endpoint).json()
        return int(res["balance"]) / config.CHAIN_DECIMALS[chain]

    # deprecated chain
    if chain.lower() == "bnb":
        return 0

    # fatal error
    raise Exception(f"Unknown chain {chain}")


########################################################################################
# Helpers
########################################################################################


@functools.cache
def geocode(ip: str):
    return requests.get(f"https://api.ninerealms.com/util/ip/{ip}").json()


@functools.cache
def whois(ip: str):
    who = ipwhois.IPWhois(ip).lookup_rdap()
    return " ".join([who["network"]["name"], who["asn_description"]])


@functools.cache
def active_validators() -> Set[str]:
    validators = set()
    for node in thorchain_nodes(thornode_latest_height()):
        if node["status"] == "Active":
            validators.add(node["node_address"])
    return validators


def churn_heights(count: int, end_height: Optional[int] = None):
    """Return the last `count` churn heights before `end_height`"""
    vaults = thorchain_vaults_asgard(end_height)

    churns = []
    for i in range(0, count):
        height = None
        for v in vaults:
            if v["status"] == "ActiveVault" and (
                not height or v["block_height"] > height
            ):
                height = v["block_height"]
        churns.append(height)
        assert type(height) == int  # lint
        if i < count - 1:
            vaults = thorchain_vaults_asgard(height - 1)

    return churns


def pretty_whois(ip):
    try:
        who = whois(ip).lower()
        if "goog" in who:
            return "google"
        if "msft" in who:
            return "azure"
        if "digitalocean" in who:
            return "digitalocean"
        if "at" in who or "aws" in who or "amazo" in who:
            return "aws"
        if "choopa" in who:
            return "vultr"
        if "hetzner" in who:
            return "hetzner"
        return who
    except:
        return None


def pubkey_address(pubkey: str, raw_hex: bool = False):
    # decode the bech32 string
    hrp, data, _ = bech32.bech32_decode(pubkey)
    if not hrp or not hrp.endswith("pub"):
        raise Exception("Invalid pubkey")

    # convert to 8 bits
    data = bech32.convertbits(data, 5, 8, False)
    assert data

    # cosmos address is the first 20 bytes of the pubkey sha256 skipping first 5 bytes
    addr = hashlib.sha256(bytearray(data[5:])).digest()[:20]

    if raw_hex:
        return addr.hex()

    # encode the address
    return bech32.bech32_encode(hrp[:-3], bech32.convertbits(addr, 8, 5, True), 1)


def nodes_dataframe(height: Optional[int] = None, mimirs=True):
    nodes = thorchain_nodes(height)

    node_mimirs = {}
    if mimirs:
        for vote in thornode_mimir_nodes().get("mimirs") or []:
            if vote["signer"] not in node_mimirs:
                node_mimirs[vote["signer"]] = {}
            try:
                node_mimirs[vote["signer"]][vote["key"]] = vote.get("value", 0)
            except KeyError:
                pass

    chains = set()

    rows = []
    for node in nodes:
        row = node

        # abbreviated address
        row["na"] = "thor..." + row["node_address"][-4:]

        # flatten dicts
        for d in ["pub_key_set", "jail", "preflight_status", "bond_providers"]:
            for k, v in (row.get(d) or {}).items():
                row[f"{d}.{k}"] = v
            del row[d]

        # flatten observe chains
        for v in row.get("observe_chains", []) or []:
            row[v["chain"]] = v["height"]
            chains.add(v["chain"])

        # convert bond to int
        if "bond" in row:
            row["bond"] = int(row["bond"])
        if "total_bond" in row:
            row["total_bond"] = int(row["total_bond"])

        try:
            row["node_mimirs"] = node_mimirs[row["node_address"]]
        except KeyError:
            row["node_mimirs"] = {}

        # add validator consensus address columns if pub key is set
        cpk = row["validator_cons_pub_key"]
        if cpk:
            row["validator_cons_addr"] = pubkey_address(cpk, True)
            row["validator_cons_addr_bech32"] = pubkey_address(cpk)

        rows.append(row)

    df = pd.DataFrame(rows)

    # add <chain>.lag columns
    for chain in chains - config.RAGNAROK_CHAINS:
        df[f"{chain}.lag"] = (df[chain].quantile(0.9) - df[chain].fillna(0)).astype(int)
        df[f"{chain}.lag"] = df.mask(df["status"] != "Active", 0)[f"{chain}.lag"]

    return df


def format_params(params: Dict):
    return "&".join([f"{k}={v}" for k, v in params.items()])


def parse_blockcypher_time(ts: str):
    cleaned_ts = ts.split("Z")[0].split(".")[0]
    return datetime.datetime.strptime(cleaned_ts, "%Y-%m-%dT%H:%M:%S")


#########################################################################################
# CustomGroup
########################################################################################


class CustomGroup(click.Group):
    def get_help(
        self,
        ctx,
    ):
        formatter = ctx.make_formatter()
        super().format_usage(ctx, formatter)
        super().format_help_text(ctx, formatter)
        click.Command.format_options(self, ctx, formatter)
        help_text = formatter.getvalue().strip()

        help_text += "\n\nCommands:\n"
        help_text += self.get_help_commands(ctx, "")
        return help_text

    def get_help_commands(self, ctx, indent) -> str:
        indent += " " * 2
        output = ""
        for subcommand in self.commands.values():
            output += click.style(
                f"{indent}{subcommand.name}",
                fg="blue" if len(indent) == 2 else None,
                bold=len(indent) == 2,
            )
            if len(indent) == 2:
                output += f": {subcommand.get_short_help_str()}"
            output += "\n"
            if hasattr(subcommand, "get_help_commands"):
                output += subcommand.get_help_commands(ctx, indent)
        return output
